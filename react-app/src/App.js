import React, { useEffect, useState } from 'react';
import './App.css';
import axios from "axios";

function App() {
  // useState
  const [customers, setCustomers] = useState([]);
  const [loading, setLoading] = useState(true);

  // este effect solo se ejecuta la primera vez que carga este componente
  useEffect(() => {
    obtenerData();
    async function obtenerData() {
      // vamos a obtener la data del servicio
      const res = await axios.get("http://localhost:50524/Services/CustomerService.asmx/ObtenerTodos", {
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        data: {}
      })
      // asignar el valor del resultado a la variable de  customers
      setCustomers(res.data.d);
      // cambiar el valor de loading a false
      setLoading(false);
    }
  }, [])

  if(loading){
    return <div>Cargando.....</div>
  }
  return (
    <div className="App">
      <h1>Lista de Clientes</h1>
      <table>
        <thead>
          <tr>
            <th>
              Id
            </th>
            <th>
              Nombres
            </th>
            <th>
              Img
            </th>
          </tr>
        </thead>
        <tbody>
          {customers.map((customer) => {
            return <tr>
              <td>
                {customer.CustomerId}
              </td>
              <td>
                {customer.FirstName} {customer.LastName}
              </td>
              <td>
                <img src={customer.ImageProfile}></img>
              </td>
            </tr>
          })}
        </tbody>
      </table>
    </div>
  );
}

export default App;
