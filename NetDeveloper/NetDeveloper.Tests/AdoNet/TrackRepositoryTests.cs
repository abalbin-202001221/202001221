﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetDeveloper.DataAccess.AdoNet;
using NetDeveloper.Models;
using System;

namespace NetDeveloper.Tests.AdoNet
{
    [TestClass]
    public class TrackRepositoryTests
    {
        [TestMethod]
        public void CountDeberRetornat0OMayorA0()
        {
            // 1. Instanciar el objeto repository
            var repo = new TrackRepository();
            // 2. Obtener el count
            var count = repo.Count();
            // 3. Hacer la comprobación
            Console.WriteLine($"La cantidad de elementos es {count}");
            Assert.AreEqual(true, count >= 0);
        }

        [TestMethod]
        public void GetAllDebeFuncionar()
        {
            // 1. Instanciar el objeto repository
            var repo = new TrackRepository();
            // 2. Ejecutar el método
            var lista = repo.GetAll();
            // 3. Hacer la comprobación           
            Assert.AreEqual(true, lista.Count > 0);
        }

        [TestMethod]
        public void GetTrackByIdDebeRetornarElValorCorrecto()
        {
            var repo = new TrackRepository();
            var track = repo.GetById(3498);
            Assert.AreEqual("Pietro Antonio Locatelli", track.Composer);
        }

        [TestMethod]
        public void InsertTrackDebeFuncionar()
        {
            var repo = new TrackRepository();
            // crear el objeto track que queremos insertar
            var newTrack = new Track
            {
                Name = "track test",
                MediaTypeId = 1,
                Milliseconds = 3000,
                UnitPrice = 2.34M
            };

            var nuevoId = repo.Insert(newTrack);
            Assert.AreEqual(true, nuevoId > 0);
        }

        [TestMethod]
        public void DeleteTrackDebeFuncionar()
        {
            var repo = new TrackRepository();
            // crear el objeto track que queremos insertar para luego eliminar
            var newTrack = new Track
            {
                Name = "track test eliminar",
                MediaTypeId = 1,
                Milliseconds = 3000,
                UnitPrice = 2.34M
            };

            var nuevoId = repo.Insert(newTrack);

            // eliminar el track creado
            var seElimino = repo.DeleteById(nuevoId);
            Assert.AreEqual(true, seElimino);
        }

        [TestMethod]
        public void DeleteTrackDebeFallar()
        {
            var repo = new TrackRepository();
            
            // eliminar un track que tenga relacione con otras tablas
            var seElimino = repo.DeleteById(1);
            Assert.AreEqual(false, seElimino);
        }
    }
}
