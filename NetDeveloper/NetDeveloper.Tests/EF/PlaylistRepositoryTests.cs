﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetDeveloper.DataAccess.EF;

namespace NetDeveloper.Tests.EF
{
    [TestClass]
    public class PlaylistRepositoryTests
    {
        private string _connectionString => "server=.;database=Chinook;user id=sa;password=sql";

        [TestMethod]
        public void CantidaDeRegistrosDebeSerMayorA0()
        {
            var repo = new PlaylistRepository(_connectionString);

            var resultado = repo.ObtenerClassicPlaylists();

            Assert.AreEqual(4, resultado.Count());
        }
    }
}
