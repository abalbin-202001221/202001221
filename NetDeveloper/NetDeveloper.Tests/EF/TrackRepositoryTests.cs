﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetDeveloper.DataAccess.EF;
using NetDeveloper.Models;
using System.Configuration;
using NetDeveloper.DataAccess;

namespace NetDeveloper.Tests.EF
{
    [TestClass]
    public class TrackRepositoryTests
    {
        private readonly ChinookContext _context;
        public TrackRepositoryTests()
        {
            // obtener la cadena de conexión del archivo de configuración
            var cs = ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString;
            _context = new ChinookContext(cs);
        }
        [TestMethod]
        public void TrackSalesReportDebeFuncionar()
        {
            // 1. Utilizar el Unit Of Work
            using (var unit = new UnitOfWork(_context))
            {
                // 2. Llamar al método que devuelve todos los registros
                var resultado = unit.Tracks.GetTrackSalesReport();

                //3. Hacer la comprobación
                Assert.AreEqual(true, resultado.Count() > 0);
            }
        }
    }
}
