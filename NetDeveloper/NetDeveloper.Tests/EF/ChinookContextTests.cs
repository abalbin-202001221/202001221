﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetDeveloper.DataAccess.EF;
using NetDeveloper.Models;

namespace NetDeveloper.Tests.EF
{
    [TestClass]
    public class ChinookContextTests
    {
        [TestMethod]
        public void DebeInsertarUnArtista()
        {
            using (var context = new ChinookContext("server=.;database=Chinook;user id=sa;password=sql;"))
            {
                context.Artists.Add(new Artist { Name = "Nuevo Artista EF" });
                context.SaveChanges();
            }
        }
    }
}
