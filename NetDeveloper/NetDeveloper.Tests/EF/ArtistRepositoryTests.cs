﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetDeveloper.DataAccess.EF;
using NetDeveloper.Models;
using System.Configuration;
using NetDeveloper.DataAccess;

namespace NetDeveloper.Tests.EF
{
    [TestClass]
    public class ArtistRepositoryTests
    {
        private readonly ChinookContext _context;
        public ArtistRepositoryTests()
        {
            // obtener la cadena de conexión del archivo de configuración
            var cs = ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString;
            _context = new ChinookContext(cs);
        }
        [TestMethod]
        public void CantidaDeRegistrosDebeSerMayorA0()
        {
            // 1. Instanciar el repo
            var repo = new ArtistRepository(_context);

            // 2. Llamar al método que devuelve todos los registros
            var resultado = repo.ObtenerTodos();

            //3. Hacer la comprobación
            Assert.AreEqual(true, resultado.Count() > 0);
        }

        [TestMethod]
        public void ArtistaConId50DebeSerMetallica()
        {
            // 1. Instanciar el repo
            var repo = new ArtistRepository(_context);

            // 2. Llamar al método para obtener el registro con id 50
            var resultado = repo.ObtenerPorId(50);

            //3. Hacer la comprobación
            Assert.AreEqual("Metallica", resultado.Name);
        }

        [TestMethod]
        public void Primeros10RegistrosDebeFuncionar()
        {
            // 1. Instanciar el repo
            var repo = new ArtistRepository(_context);

            // 2. Llamar al método para obtener los 10 primeros registro ordenaods por id
            var resultado = repo.ObtenerNPrimeros(10);

            //3. Hacer la comprobación
            Assert.AreEqual(true, resultado.Count() == 10);
        }

        [TestMethod]
        public void Insertar1000ArtistasMasivo()
        {
            using (var unit = new UnitOfWork(_context))
            {
                // 2. Crear los 100 artistas a insertar
                var artistas = new List<Artist>();
                for (int i = 0; i < 1000; i++)
                {
                    artistas.Add(new Artist { Name = $"Artista masivo {i + 1}" });
                }

                // enviar los 1000 artistas a insertar
                unit.Artists.InsertarMasivo(artistas);

                // guardar los cambios
                var resultadoMasivo = unit.Complete();

                // Limpiar la base de datos (eliminar los registros que empiezan con Artista masivo
                unit.Artists.EliminarPorNombreEmpiezaCon("Artista masivo");

                //3. Hacer la comprobación
                Assert.AreEqual(true, resultadoMasivo > 0);
            }
        }

        [TestMethod]
        public void EliminarUnRegistroDebeRetornar1()
        {
            using (var unit = new UnitOfWork(_context))
            {
                var newArtist = new Artist { Name = "Artista a eliminar" };
                // 2. Insertar un registro de prueba que luego será eliminado
                unit.Artists.Insertar(newArtist);

                unit.Complete();

                // 3. Eliminar el registro con el nuevo id
                unit.Artists.Eliminar(newArtist.ArtistId);

                var resultadoEliminar = unit.Complete();

                Console.WriteLine($"Cantidad de registros eliminados {resultadoEliminar}");

                // 4. Hacer la comprobación
                Assert.AreEqual(1, resultadoEliminar);
            }
        }

        [TestMethod]
        public void ActualizarNuevoArtistaInsertadoDebeRetornar1()
        {
            using (var unit = new UnitOfWork(_context))
            {
                var newArtist = new Artist { Name = "Artista nuevo" };
                // 2. Insertar un registro de prueba que luego será eliminado
                unit.Artists.Insertar(newArtist);

                unit.Complete();

                // 3. Actualizar el nombre
                newArtist.Name = "Artista nuevo actualizado";

                var resultadoActualizar = unit.Complete();

                Console.WriteLine($"Cantidad de registros actualizados {resultadoActualizar}");

                // 4. Hacer la comprobación
                Assert.AreEqual(1, resultadoActualizar);
            }
        }

        [TestMethod]
        public void ObtenerArtistasYCantdeAlbumesDebeRetornarMasDe0()
        {
            // 1. Crea el repositorio
            var repo = new ArtistRepository(_context);

            // 2. obtener la data
            var resultado = repo.ObtenerCantidadDeAlbumesPorArtista();

            // 3. hacer la comprobación
            Assert.AreEqual(true, resultado.Count() > 0);
            Assert.AreEqual(true, resultado[0].AlbumCount > 0);
        }

        [TestMethod]
        public void InsertarArtistaConAlbumesDebeFuncionar()
        {
            using (var unit = new UnitOfWork(_context))
            {
                // 2. Crear el objeto Artista que queremos insertar
                var newArtist = new Artist { Name = "Artista con Álbumes Test" };

                // 2.1. Agregar los álbumes que queremos insertar como detalle del artista
                newArtist.Albums = new Album[] {
                new Album { Title = "Álbum de prueba 1"},
                new Album { Title = "Álbum de prueba " } };

                // 3. Insertar el nuevo artista a la BD
                unit.Artists.Insertar(newArtist);

                unit.Complete();

                // 4. Obtener los albumes del artista            
                var albumesArtista = unit.Albums.ObtenerPorArtistId(newArtist.ArtistId);

                Assert.AreEqual(2, albumesArtista.Count());
            }
        }
    }
}
