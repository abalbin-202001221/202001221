﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetDeveloper.DataAccess;
using NetDeveloper.DataAccess.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using NetDeveloper.Models;

namespace NetDeveloper.Tests
{
    [TestClass]
    public class UniOfWorkTests
    {
        private readonly ChinookContext _context;
        public UniOfWorkTests()
        {
            // obtener la cadena de conexión del archivo de configuración
            var cs = ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString;
            _context = new ChinookContext(cs);
        }

        [TestMethod]
        public void ObtenerPlaylistsDebeRetornarMasDe0Elementos()
        {
            using (var unit = new UnitOfWork(_context))
            {
                var resultado = unit.Playlists.ObtenerTodos();
                Assert.IsInstanceOfType(resultado, typeof(IEnumerable<Playlist>));
                Assert.AreEqual(true, resultado.Count() > 0);
            }
        }

        [TestMethod]
        public void ObtenerArtistasDebeRetornarMasDe0Elementos()
        {
            using (var unit = new UnitOfWork(_context))
            {
                var resultado = unit.Artists.ObtenerTodos();
                Assert.IsInstanceOfType(resultado, typeof(IEnumerable<Artist>));
                Assert.AreEqual(true, resultado.Count() > 0);
            }
        }

        [TestMethod]
        public void ObtenerGenerosDebeRetornarMasDe0Elementos()
        {
            using (var unit = new UnitOfWork(_context))
            {
                var resultado = unit.Genres.ObtenerTodos();
                Assert.IsInstanceOfType(resultado, typeof(IEnumerable<Genre>));
                var resultadoComoLista = resultado.ToList();
                Assert.AreEqual(true, resultadoComoLista.Count > 0);
            }
        }

        [TestMethod]
        public void ObtenerCustomersDebeRetornarMasDe0Elementos()
        {
            using (var unit = new UnitOfWork(_context))
            {
                var resultado = unit.Customers.ObtenerTodos();
                Assert.IsInstanceOfType(resultado, typeof(IEnumerable<Customer>));
                Assert.AreEqual(true, resultado.Count() > 0);
            }
        }

        [TestMethod]
        public void ObtenerEmployeesDebeRetornarMasDe0Elementos()
        {
            using (var unit = new UnitOfWork(_context))
            {
                var resultado = unit.Employees.ObtenerTodos();
                Assert.IsInstanceOfType(resultado, typeof(IEnumerable<Employee>));
                Assert.AreEqual(true, resultado.Count() > 0);
            }
        }

        [TestMethod]
        public void InsertarCustomerDebeFuncionar()
        {
            using (var unit = new UnitOfWork(_context))
            {
                // crear nuevo empleado
                var newEmployee = new Employee
                {
                    FirstName = "Nuevo",
                    LastName = "Empleado"
                };
                unit.Employees.Insertar(newEmployee);

                // guardar cambios
                unit.Complete();

                // crear el customer
                var newCustomer = new Customer
                {
                    FirstName = "Nuevo",
                    LastName = "Customer",
                    SupportRepId = newEmployee.EmployeeId,
                    Email = "correo@mail.com"
                };

                unit.Customers.Insertar(newCustomer);

                // guardar los cambios
                unit.Complete();

                // obtener el nuevo customer por id
                var customer = unit.Customers.ObtenerPorId(newCustomer.CustomerId);

                Assert.AreEqual("Nuevo", customer.FirstName);
            }
        }

        [TestMethod]
        public void InsertarArtistaPorSPDebeFuncionar()
        {
            using(var unit = new UnitOfWork(_context))
            {
                var result = unit.Artists.InsertWithSP(new Artist { Name = "Artist SP From Test" });
                Assert.AreEqual(result > 0, true);
            }
        }

        [TestMethod]
        public void ObtenerUsuarioPorCorreoYPassDebeFuncionar()
        {
            using(var unit = new UnitOfWork(_context))
            {
                var result = unit.Users.ObtenerUsuarioPorEmailPass("usuario1@mail.com", "123456");
                Assert.AreEqual(result.Dni, "70433063");
            }
        }
    }
}
