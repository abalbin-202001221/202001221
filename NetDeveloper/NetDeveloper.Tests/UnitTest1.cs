﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NetDeveloper.Tests
{
    class Persona : IDisposable
    {
        public void Dispose()
        {
            Console.WriteLine("QEPD");
        }
    }

    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            // La prueba va a comprobar que 1 es igual a 1
            Assert.AreEqual(1, 1);
        }

        [TestMethod]
        public void TestMethod2()
        {
            // hacer una suma de 100 + 200
            var suma = 100 + 200;
            Assert.AreEqual(300, suma);
        }

        [TestMethod]
        public void TestMethod3()
        {
            using (var persona = new Persona())
            {
                Assert.AreEqual(true, true);
            }
        }

        [TestMethod]
        public void TestMethod4()
        {
            var id = 0;
            var mensaje = "";
            if (id < 0)
                mensaje = "valor negativo";
            else
                mensaje = "valor positivo";
            Console.WriteLine(mensaje);

            // otra forma
            Console.WriteLine(id < 0 ? "valor negativo" : "valor positivo");

            // otra forma
            Console.WriteLine($"valor {(id < 0 ? "negavtivo" : "positivo")}");

            Assert.AreEqual(true, true);
        }
    }
}
