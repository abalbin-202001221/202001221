﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetDeveloper.DataAccess.Interfaces;
using NetDeveloper.Models;
using NetDeveloper.Models.Reports;

namespace NetDeveloper.DataAccess.EF
{
    public class ArtistRepository : Repository<Artist>, IArtistRepository
    {
        public ArtistRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<Artist> ObtenerNPrimeros(int n)
        {
            return _context.Set<Artist>().OrderBy(a => a.ArtistId).Take(n).ToList();
        }

        public void InsertarMasivo(List<Artist> artistasNuevos)
        {
            // agregar la nueva entidad al DbSet
            _context.Set<Artist>().AddRange(artistasNuevos);
        }

        public void EliminarPorIdYNombre(int id, string nombre)
        {
            // obtener el artista que queremos eliminar
            var artista = _context.Set<Artist>().FirstOrDefault(a => a.ArtistId == id && a.Name == nombre);

            _context.Set<Artist>().Remove(artista);
        }

        public void EliminarPorNombreEmpiezaCon(string cadena)
        {
            // 1. Obtener todos los registros que queremos eliminar
            var artistasEliminar = _context.Set<Artist>().Where(a => a.Name.ToLower().StartsWith(cadena.ToLower()));

            // 2. eliminar todos los registros obtenido en el paso 1                
            _context.Set<Artist>().RemoveRange(artistasEliminar);
        }

        public List<ArtistAlbumCountReport> ObtenerCantidadDeAlbumesPorArtista()
        {
            var query = _context.Set<Artist>()
                .Select(a => new ArtistAlbumCountReport
                {
                    ArtistId = a.ArtistId,
                    ArtistName = a.Name,
                    AlbumCount = a.Albums.Count
                });
            return query.ToList();
        }

        public int InsertWithSP(Artist newArtist)
        {
            var nameParam = new SqlParameter
            {
                ParameterName = "name",
                Value = newArtist.Name,
            };
            var result = _context.Database.SqlQuery<int>("exec spInsertArtist @name", nameParam);
            return result.First();
        }
    }
}
