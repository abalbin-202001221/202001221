﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetDeveloper.Models;
using NetDeveloper.Models.Reports;
using System.Data.Entity;
using NetDeveloper.DataAccess.Interfaces;

namespace NetDeveloper.DataAccess.EF
{
    public class AlbumRepository : Repository<Album>, IAlbumRepository
    {
        public AlbumRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<AlbumArtistReport> ObtenerPorArtistId(int artistId)
        {
            var resultado = _context.Set<Album>()
                .Where(a => a.ArtistId == artistId)
                .Select(a => new AlbumArtistReport { AlbumTitle = a.Title, ArtistName = a.Artist.Name });
            return resultado.ToList();
        }
    }
}
