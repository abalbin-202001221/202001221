﻿using NetDeveloper.DataAccess.Interfaces;
using NetDeveloper.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.DataAccess.EF
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(DbContext context) : base(context)
        {
        }

        public User ObtenerUsuarioPorEmailPass(string email, string password)
        {
            return _context.Set<User>().Include(u => u.Customer).FirstOrDefault(u => u.Email == email && u.Password == password);
        }
    }
}
