﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using NetDeveloper.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace NetDeveloper.DataAccess.EF
{
    public class ChinookContext : DbContext
    {
        public bool IsDisposed { get; set; }
        // constructor para inicializar la conexión con la BD
        public ChinookContext(string cadena) : base(cadena)
        {
            // desactivar la inicialización de la BD (para que no trate de crear una BD nueva)
            Database.SetInitializer<ChinookContext>(null);
            IsDisposed = false;
        }

        protected override void Dispose(bool disposing)
        {
            IsDisposed = true;
            base.Dispose(disposing);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // esta linea llama al modelCreating del padre
            base.OnModelCreating(modelBuilder);

            //desactivar la convención de pluralización de tablas
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //modelBuilder.Entity<Customer>().Property((p) => p.ImageProfile).HasColumnName("Img_Profile");
        }

        // definir los DBSet (tablas) con los que se va a trabajar en este contexto
        public DbSet<Artist> Artists { get; set; }
        public DbSet<Playlist> Playlists { get; set; }
        public DbSet<Album> Albums { get; set; }
        public DbSet<Track> Tracks { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
