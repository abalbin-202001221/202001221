﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetDeveloper.Models;
using NetDeveloper.Models.Reports;
using System.Data.Entity;
using NetDeveloper.DataAccess.Interfaces;

namespace NetDeveloper.DataAccess.EF
{
    public class TrackRepository : Repository<Track>, ITrackRepository
    {
        public TrackRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<Track> GetByAlbumId(int albumId)
        {
            return _context.Set<Track>().Where(t => t.AlbumId == albumId);
        }

        public IEnumerable<TrackSalesReport> GetTrackSalesReport()
        {
            var resultado = _context.Set<Track>()
                .Select(t => new TrackSalesReport
                {
                    TrackId = t.TrackId,
                    Name = t.Name,
                    SalesCount = t.InvoiceLines.Count == 0 ? 0 : t.InvoiceLines.Sum(i => i.Quantity)
                });
            return resultado.ToList();
        }
    }
}
