﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetDeveloper.DataAccess.EF;
using NetDeveloper.Models;

namespace NetDeveloper.DataAccess.EF
{
    public class PlaylistRepository
    {
        private readonly string _connectionString;
        public PlaylistRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IEnumerable<Playlist> ObtenerClassicPlaylists()
        {
            using (var context = new ChinookContext(_connectionString))
            {
                return context.Playlists
                    .Where(p => p.Name.ToLower().StartsWith("classic")).ToList();
            }
        }        
    }
}
