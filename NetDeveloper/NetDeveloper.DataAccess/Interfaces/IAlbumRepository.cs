﻿using NetDeveloper.Models;
using NetDeveloper.Models.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.DataAccess.Interfaces
{
    public interface IAlbumRepository : IRepository<Album>
    {
        IEnumerable<AlbumArtistReport> ObtenerPorArtistId(int artistId);
    }
}
