﻿using NetDeveloper.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.DataAccess.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        // definir los repositorios
        IArtistRepository Artists { get; }
        IAlbumRepository Albums { get; }
        ITrackRepository Tracks { get; }
        IRepository<Playlist> Playlists { get; }
        IRepository<Genre> Genres { get; }
        IRepository<Customer> Customers { get; }
        IRepository<Employee> Employees { get; }
        IRepository<Invoice> Invoices { get; }
        IUserRepository Users { get; }

        // métodos para el manejo del contexto
        int Complete();
    }
}
