﻿using System.Collections.Generic;
using NetDeveloper.Models;
using NetDeveloper.Models.Reports;

namespace NetDeveloper.DataAccess.Interfaces
{
    public interface IArtistRepository : IRepository<Artist>
    {        
        void EliminarPorIdYNombre(int id, string nombre);
        void EliminarPorNombreEmpiezaCon(string cadena);
        void InsertarMasivo(List<Artist> artistasNuevos);
        List<ArtistAlbumCountReport> ObtenerCantidadDeAlbumesPorArtista();
        IEnumerable<Artist> ObtenerNPrimeros(int n);
        int InsertWithSP(Artist newArtist);
    }
}