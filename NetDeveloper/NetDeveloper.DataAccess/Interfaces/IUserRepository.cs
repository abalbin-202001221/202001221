﻿using NetDeveloper.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.DataAccess.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        // Este metodo permirtira obtener al usuario de BD
        User ObtenerUsuarioPorEmailPass(string email, string password);
    }
}
