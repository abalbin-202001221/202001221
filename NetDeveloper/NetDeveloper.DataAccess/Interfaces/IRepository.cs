﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.DataAccess.Interfaces
{     
    public interface IRepository<T> where T : class
    {
        // definir todos los métodos de esta interfaz
        T ObtenerPorId(int id);
        IEnumerable<T> ObtenerTodos();
        void Insertar(T nuevaEntidad);
        void Actualizar(T entidad);
        void Eliminar(int id);
    }
}
