﻿using NetDeveloper.Models;
using NetDeveloper.Models.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.DataAccess.Interfaces
{
    public interface ITrackRepository : IRepository<Track>
    {
        IEnumerable<TrackSalesReport> GetTrackSalesReport();
        IEnumerable<Track> GetByAlbumId(int albumId);
    }
}
