﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Dapper;
using NetDeveloper.Models;
using NetDeveloper.Models.Reports;

namespace NetDeveloper.DataAccess.Dapper
{
    public class TrackRepository
    {
        private readonly string _connectionString;
        public TrackRepository()
        {
            // cuando se cree una nueva instancia de esta clase, asignar el valor a _connectionString
            _connectionString = "server=.;database=Chinook;user id=sa;password=sql;";
        }

        public int Count()
        {
            // crear el query
            var query = "select count(1) from Track";

            // realizar la consulta
            using (var connection = new SqlConnection(_connectionString))
            {
                var count = connection.ExecuteScalar<int>(query);
                return count;
            }
        }

        public List<Track> GetAll()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Track>("select * from Track").ToList();
            }
        }

        public Track GetByIdSP(int trackId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                // definir los parámetro del SP
                var parametros = new { trackId };

                // retornar el valor de la consulta
                return connection.QueryFirstOrDefault<Track>("spGetTrackById", parametros, commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public Track GetById(int trackId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                // definir el query para obtener el registro
                var query = "select * from Track where TrackId = @trackId";
                // definir los parámetro del SP
                var parametros = new { trackId };

                // retornar el valor de la consulta
                return connection.QueryFirstOrDefault<Track>(query, parametros);
            }
        }

        public int Insert(Track newTrack)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var parametros = new
                {
                    name = newTrack.Name,
                    mediaTypeId = newTrack.MediaTypeId,
                    milliseconds = newTrack.Milliseconds,
                    unitPrice = newTrack.UnitPrice
                };
                return connection.ExecuteScalar<int>("spInsertTrack", parametros, commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public bool DeleteById(int trackId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var parametros = new
                {
                    trackId
                };

                try
                {
                    var resultado = connection.ExecuteScalar<int>("spDeleteTrack", parametros, commandType: System.Data.CommandType.StoredProcedure);

                    return resultado > 0;
                }
                catch (Exception)
                {
                    return false;
                }

            }
        }

        public IEnumerable<TrackSalesReport> GetTrackSalesReport()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<TrackSalesReport>("spTrackSalesReport", commandType: System.Data.CommandType.StoredProcedure);
            }
        }
    }
}