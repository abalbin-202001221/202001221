﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using NetDeveloper.Models;

namespace NetDeveloper.DataAccess.AdoNet
{
    public class TrackRepository
    {
        private readonly string _connectionString;
        public TrackRepository()
        {
            // cuando se cree una nueva instancia de esta clase, asignar el valor a _connectionString
            _connectionString = "server=.;database=Chinook;user id=sa;password=sql;";
        }
        /// <summary>
        /// Función que consulta la cantidad de elementos de la tabla Track
        /// </summary>
        /// <returns>La cantidad de elementos</returns>
        public int Count()
        {
            // 1. Crear una variable que tenga el query TSQL que queremos ejecutar
            var query = "select count(1) from Track";

            // 2. Instanciar la conexión
            // . = localhost = local
            //var connectionString = "server=.;database=Chinook;user id=sa;password=sql;";
            var connection = new SqlConnection(_connectionString);

            // 3. Crear el comando que se va a ejecutar
            var command = new SqlCommand(query, connection);

            // 4. Abrir la conexión a la BD
            connection.Open();

            // 5. Ejecutar el comando y guardarlo en el valor del count
            var count = (int)command.ExecuteScalar();

            // 6. Cerrar la conexión
            connection.Close();

            // 7. Retornar count
            return count;
        }

        /// <summary>
        /// Obtener todos los registros de la tabla Track
        /// </summary>
        public List<Track> GetAll()
        {
            // 1. Declarar el query SQL que se va a ejecutar
            var query = "select * from Track";

            // 2. Instanciar la conexión            
            using (var connection = new SqlConnection(_connectionString))
            {
                // 2.1. Abrir la conexión
                connection.Open();

                // 2.2. Crear el SQLCommand
                var cmd = new SqlCommand(query, connection);

                // 2.3. Crear el DataReader (para leer la data resultante fila x fila)
                var reader = cmd.ExecuteReader();

                // 2.4. Ejecutar un while para poder lees fila x fila
                // Este loop se va a ejecutar mientras el reader tenga filas que leer
                var listaTracks = new List<Track>();
                while (reader.Read())
                {
                    // instanciamos un objeto para obtener todos los valores que resultan del reader
                    var currentTrack = new Track
                    {
                        // el parámetro que se le indica al reader es el NOMBRE de la columna que resulta del query
                        TrackId = Convert.ToInt32(reader["TrackId"]),
                        Name = reader["Name"].ToString(),
                        // para manejar el caso de que el valor sea NULL
                        AlbumId = (reader["AlbumId"] as int?) ?? 0,
                        MediaTypeId = Convert.ToInt32(reader["MediaTypeId"]),
                        GenreId = (reader["GenreId"] as int?) ?? 0,
                        Composer = reader["Composer"].ToString(),
                        Milliseconds = Convert.ToInt32(reader["Milliseconds"]),
                        Bytes = (reader["Bytes"] as int?) ?? 0,
                        UnitPrice = Convert.ToDecimal(reader["UnitPrice"]),
                    };

                    // agregar el objeto a la lista
                    listaTracks.Add(currentTrack);
                }

                // 2.5. Retornar la lista de track
                return listaTracks;
            }

        }

        /// <summary>
        /// Obtiene un registro de la tabla Track en base a su ID. Utiliza el store procedure llamado spGetTrackById
        /// </summary>
        /// <returns></returns>
        public Track GetById(int trackId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                // crear el comando para ejecutar un stored procedure
                var cmd = new SqlCommand("spGetTrackById", connection);
                // indicar el tipo de comando
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                // agregar los parámetros
                //var parametroTrackId = new SqlParameter {
                //    Value = trackId,
                //    ParameterName = "@trackId",
                //    SqlDbType = System.Data.SqlDbType.Int
                //};
                //cmd.Parameters.Add(parametroTrackId);
                //cmd.Parameters.Add("@trackId", System.Data.SqlDbType.Int).Value = trackId;
                cmd.Parameters.AddWithValue("@trackId", trackId);

                // leer los resultado con el data reader
                connection.Open();
                var reader = cmd.ExecuteReader();

                // crear un nuevo objeto track para traer la data del resultado del stored procedure
                var track = new Track();
                while (reader.Read())
                {
                    track.TrackId = Convert.ToInt32(reader["TrackId"]);
                    track.Name = reader["Name"].ToString();
                    track.AlbumId = (reader["AlbumId"] as int?) ?? 0;
                    track.MediaTypeId = Convert.ToInt32(reader["MediaTypeId"]);
                    track.GenreId = (reader["GenreId"] as int?) ?? 0;
                    track.Composer = reader["Composer"].ToString();
                    track.Milliseconds = Convert.ToInt32(reader["Milliseconds"]);
                    track.Bytes = (reader["Bytes"] as int?) ?? 0;
                    track.UnitPrice = Convert.ToDecimal(reader["UnitPrice"]);
                }

                // retornar el objeto track
                return track;
            }
        }

        /// <summary>
        /// Inserta una canción y devuleve el nuevo ID generado
        /// </summary>
        /// <returns></returns>
        public int Insert(Track newTrack)            
        {
            using(var connection = new SqlConnection(_connectionString))
            {
                var cmd = new SqlCommand("spInsertTrack", connection)
                {
                    CommandType = System.Data.CommandType.StoredProcedure
                };

                // agregar los parámetros
                cmd.Parameters.AddWithValue("@name", newTrack.Name);
                cmd.Parameters.AddWithValue("@mediaTypeId", newTrack.MediaTypeId);
                cmd.Parameters.AddWithValue("@milliseconds", newTrack.Milliseconds);
                cmd.Parameters.AddWithValue("@unitPrice", newTrack.UnitPrice);

                // retornar el nuevo ID
                connection.Open();
                return (int)cmd.ExecuteScalar();
            }
        }

        /// <summary>
        /// Elimina una canción en base a su ID
        /// </summary>
        /// <returns></returns>
        public bool DeleteById(int trackId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var cmd = new SqlCommand("spDeleteTrack", connection)
                {
                    CommandType = System.Data.CommandType.StoredProcedure
                };

                // agregar los parámetros
                cmd.Parameters.AddWithValue("@trackId", trackId);
                
                connection.Open();
                try
                {
                    var resultadoEliminar = (int)cmd.ExecuteScalar();

                    // si el resultado de eliminar es mayor a 0 significa quie se eliminó el registro
                    return resultadoEliminar > 0;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
    }
}
