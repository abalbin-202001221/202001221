﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetDeveloper.DataAccess.EF;
using NetDeveloper.DataAccess.Interfaces;
using NetDeveloper.Models;

namespace NetDeveloper.DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _context;

        public UnitOfWork(DbContext context)
        {
            _context = context;

            // inicializar los repositorios
            Artists = new ArtistRepository(_context);
            Albums = new AlbumRepository(_context);
            Tracks = new TrackRepository(_context);
            Playlists = new Repository<Playlist>(_context);
            Genres = new Repository<Genre>(_context);
            Customers = new Repository<Customer>(_context);
            Employees = new Repository<Employee>(_context);
            Users = new UserRepository(_context);
            Invoices = new Repository<Invoice>(_context);
        }

        public IArtistRepository Artists { get; private set; }
        public IAlbumRepository Albums { get; private set; }
        public ITrackRepository Tracks { get; private set; }
        public IRepository<Playlist> Playlists { get; private set; }
        public IRepository<Genre> Genres { get; private set; }
        public IRepository<Invoice> Invoices { get; private set; }
        public IRepository<Customer> Customers { get; private set; }
        public IRepository<Employee> Employees { get; private set; }
        public IUserRepository Users { get; set; }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
