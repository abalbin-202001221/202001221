﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using NetDeveloper.DataAccess.Interfaces;

namespace NetDeveloper.DataAccess
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected readonly DbContext _context;

        // en el constructor del repositorio, inicializar el contexto
        public Repository(DbContext context)
        {
            _context = context;
        }

        public T ObtenerPorId(int id)
        {
            // var repo = new Repository<Artist>();
            // _context.Set<Track>().Find() = _context.Tracks.Find()
            return _context.Set<T>().Find(id);
        }

        //var repo = new Repository<Artist>()

        public IEnumerable<T> ObtenerTodos()
        {
            return _context.Set<T>();
        }

        public void Actualizar(T entidad)
        {
            _context.Entry<T>(entidad).State = EntityState.Modified;
        }

        public void Eliminar(int id)
        {
            var entidadEliminar = ObtenerPorId(id);
            _context.Set<T>().Remove(entidadEliminar);
        }

        public void Insertar(T nuevaEntidad)
        {
            _context.Set<T>().Add(nuevaEntidad);
        }
    }
}
