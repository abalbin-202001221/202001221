﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// La plantilla de elemento Página en blanco está documentada en https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0xc0a

namespace App3
{
    public class Track
    {
        public int TrackId { get; set; }
        public string Name { get; set; }
    }
    /// <summary>
    /// Página vacía que se puede usar de forma independiente o a la que se puede navegar dentro de un objeto Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public List<Track> ListaTracks { get; set; }        
        public MainPage()
        {
            this.InitializeComponent();

            // inicializar la lista con data

            // obtener la data del servicio web
            var cliente = new HttpClient();

            var respuesta = cliente.GetAsync("http://localhost:51576/api/artist").Result;

            var respuestaTexto = respuesta.Content.ReadAsStringAsync().Result;

            // Deserializar la respuesta que está en JSON
            var respuestaDeserializada = JsonConvert.DeserializeObject<List<Track>>(respuestaTexto);

            ListaTracks = respuestaDeserializada;

            //ListaView.ItemsSource = ListaTracks;
        }
    }
}
