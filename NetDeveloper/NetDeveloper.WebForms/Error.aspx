﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="NetDeveloper.WebForms.Error" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Ha ocurrido un error</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/01b3dd551d.js" crossorigin="anonymous"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="d-flex align-items-center flex-column p-4">
            <i class="fas fa-bug fa-10x text-warning"></i>
            <h1>Oh no! ha ocurrido un error
            </h1>
            <h2>Vuelve a intentar. Si el problema persiste comunicate a <a href="mailto:algun@correo.com">algun@correo.com</a>
            </h2>
        </div>
    </form>
</body>
</html>
