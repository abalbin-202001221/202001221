﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace NetDeveloper.WebForms.App_Code
{
    public class MasterPageAuth : MasterPage
    {
        protected bool IsAuthenticated()
        {
            return HttpContext.Current.User.Identity.IsAuthenticated;
        }

        /// <summary>
        /// Esta funcion va a devolver verdadero si el usuario logueado posee el rol que le estamos enviando
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        protected bool IsAuthorized(string role)
        {
            return HttpContext.Current.User.IsInRole(role);
        }

        protected void VerifyAuth()
        {
            if (!IsAuthenticated())
            {
                Response.StatusCode = 401;
                Response.End();
            }
        }
    }
}