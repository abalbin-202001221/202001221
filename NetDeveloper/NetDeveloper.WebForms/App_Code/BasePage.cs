﻿using NetDeveloper.DataAccess.EF;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace NetDeveloper.WebForms.App_Code
{
    public class BasePage : Page
    {
        private ChinookContext _context;

        public ChinookContext chinookContext
        {
            get
            {
                if(_context == null || _context.IsDisposed)
                {
                    _context = new ChinookContext(ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString);
                }
                return _context;
            }
        }

        protected void Page_Error(object sender, EventArgs e)
        {
            // este evento se va a disparar cada vez que ocurra alguna excepcion en esta pagina
            var ultimaExcepcion = Server.GetLastError().GetBaseException();
            //Response.Write($"Ha ocurrido un error con el nombre: {ultimaExcepcion.Message}");
            // loggear el error utilizando log4net
            var logger = log4net.LogManager.GetLogger(Page.GetType());
            logger.Error(ultimaExcepcion);
            Server.ClearError();
            // redireccionar a nuestra pagina de error
            Response.Redirect("~/Error.aspx");
        }
    }
}