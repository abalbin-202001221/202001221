﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetDeveloper.WebForms.App_Code
{
    public class BasePageAuth : BasePage
    {
        protected bool IsAuthenticated()
        {
            return HttpContext.Current.User.Identity.IsAuthenticated;
        }

        /// <summary>
        /// Esta funcion va a devolver verdadero si el usuario logueado posee el rol que le estamos enviando
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        protected bool IsInRole(string role)
        {
            return HttpContext.Current.User.IsInRole(role);
        }

        protected void VerifyAuth()
        {
            if (!IsAuthenticated())
            {
                Response.StatusCode = 401;
                Response.End();
            }
        }
    }
}