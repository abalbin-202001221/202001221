﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace NetDeveloper.WebForms.App_Code
{
    public class StoreBasePage : BasePage
    {
        public HttpCookie CartCookie
        {
            get
            {
                var cookie = Request.Cookies.Get("cartCookie");               
                return cookie;
            }
        }
    }
}