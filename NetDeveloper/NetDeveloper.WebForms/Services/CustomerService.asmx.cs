﻿using NetDeveloper.DataAccess;
using NetDeveloper.DataAccess.EF;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace NetDeveloper.WebForms.Services
{
    /// <summary>
    /// Summary description for CustomerService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class CustomerService : System.Web.Services.WebService
    {
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public object DevuelveObjeto()
        {
            return new { Nombre = "Algun nombre", Edad = 123 };
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public object DevuelveArreglo()
        {
            return new List<object> { new { Nombre = "Otro Nombre", Edad = 12 }, new { Nombre = "Otro Nombre 2", Edad = 22 } };
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public object ObtenerTodos()
        {
            var context = new ChinookContext(ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString);
            System.Threading.Thread.Sleep(3000);
            using (var uow = new UnitOfWork(context))
            {
                return uow.Customers.ObtenerTodos().ToList();
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = false)]
        public int Insertar(Models.Customer customer)
        {
            var context = new ChinookContext(ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString);
            using (var uow = new UnitOfWork(context))
            {
                uow.Customers.Insertar(customer);
                var resultado = uow.Complete();

                // si se inserto satisfactoriamente, devolver el nuevo id
                // en cualquier otro caso, devolver -1
                return resultado > 0 ? customer.CustomerId : -1;
            }
        }
    }
}
