﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="NetDeveloper.WebForms.Account.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Iniciar Sesion</title>
    <meta name="viewport" content="width=device-width" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous" />
    <link href="../Content/signin.css" rel="stylesheet" />
</head>
<body>
    <form runat="server" class="form-signin">
        <h1 class="h3 mb-3 font-weight-normal">Iniciar Sesion</h1>
        <p class="text-danger small" runat="server" id="pError" visible="false"></p>
        <label for="inputEmail" class="sr-only">Email address</label>
        <asp:TextBox runat="server" type="email" ID="inputEmail" CssClass="form-control" placeholder="Email address" autofocus></asp:TextBox>
        <label for="inputPassword" class="sr-only">Password</label>
        <asp:TextBox runat="server" type="password" ID="inputPassword" CssClass="form-control" placeholder="Password"></asp:TextBox>
        <asp:Button runat="server" CssClass="btn btn-lg btn-primary btn-block" Text="Iniciar" ID="btnIniciar" OnClick="btnIniciar_Click"></asp:Button>
        <asp:Button runat="server" CssClass="btn btn-lg btn-danger btn-block" Text="Iniciar con Google" ID="btnIniciarGoogle" OnClick="btnIniciarGoogle_Click" CausesValidation="false"></asp:Button>
        <p class="mt-5 mb-3 text-muted">&copy; 2020</p>
    </form>
</body>
</html>
