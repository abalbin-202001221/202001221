﻿using NetDeveloper.DataAccess;
using NetDeveloper.WebForms.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetDeveloper.WebForms.Account
{
    public partial class Login : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnIniciar_Click(object sender, EventArgs e)
        {
            // obtener los datos de inicio de sesion
            var email = inputEmail.Text;
            var pass = inputPassword.Text;

            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(pass))
            {
                pError.InnerText = "Por favor, ingresa ambos campos";
                pError.Visible = true;
                return;
            }

            // obtener el usuario de la BD
            Models.User user;
            using (var uow = new UnitOfWork(chinookContext))
            {
                user = uow.Users.ObtenerUsuarioPorEmailPass(email, pass);
            }

            if (user == null)
            {
                pError.InnerText = "El correo y/o el password no son validos.";
                pError.Visible = true;
                return;
            }

            // crear la identidad del usuario claims
            var identity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Name, user.Name),
                new Claim(ClaimTypes.NameIdentifier, user.UserId.ToString()),
                new Claim("DNI", user.Dni),
                new Claim("CustomerId", user.CustomerId.HasValue ? user.CustomerId.ToString() : ""),
                new Claim("ImageProfile", user.Customer != null ? user.Customer.ImageProfile : "")
            }, "ApplicationCookie");

            // agregar roles
            // obtener el arreglo de roles
            var rolesArray = user.Roles.Split(',');

            foreach (var role in rolesArray)
            {
                identity.AddClaim(new Claim(ClaimTypes.Role, role));
            }

            // Iniciar sesion
            // obtener el contexto de autenticacion
            var authContext = Request.GetOwinContext().Authentication;

            // con el contexto, iniciar sesion
            authContext.SignIn(identity);

            if (Request.QueryString["ReturnUrl"] != null)
            {
                Response.Redirect(Request.QueryString["ReturnUrl"]);
            }

            // redireccionar a alguna pagina
            Response.Redirect("~/Pages/Dashboard");
        }

        protected void btnIniciarGoogle_Click(object sender, EventArgs e)
        {
            // hacer el challenge con Google
            Context.GetOwinContext().Authentication.Challenge(new Microsoft.Owin.Security.AuthenticationProperties
            {
                RedirectUri = ResolveUrl("~/Account/LoginExternalCallback")
            }, "Google");
        }
    }
}