﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetDeveloper.WebForms.Account
{
    public partial class LoginExternalCallback : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // cuando cargue la pagina, hacer la logica de inicio sesion
            // utilizando la informacion que nos devuelve Google
            if (!IsPostBack)
            {
                // Obtener la inforormacion que nos devuelve el proveedor de autenticacion
                var googleLoginInfo = Context.GetOwinContext().Authentication.GetExternalLoginInfo();

                // si la informacion no existe, redirecciona nuevamente al login
                if (googleLoginInfo == null)
                {
                    Response.Redirect("~/Account/Login");
                    return;
                }

                // obtener los claims de google
                var googleClaims = googleLoginInfo.ExternalIdentity.Claims.ToList();

                var emailClaim = googleClaims.FirstOrDefault(c => c.Type == ClaimTypes.Email).Value;
                var nameClaim = googleClaims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value;
                var nameIdClaim = googleClaims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;

                // con los claims de google vamos a crear nuestra identidad
                var identidad = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Email, emailClaim),
                    new Claim(ClaimTypes.Name, nameClaim),
                    new Claim(ClaimTypes.NameIdentifier, nameIdClaim),
                    new Claim("DNI", "00000000")
                }, "ApplicationCookie");

                // agregar un rol por defecto
                identidad.AddClaim(new Claim(ClaimTypes.Role, "admin"));

                // iniciar sesion en el aplicativo
                var authContext = Request.GetOwinContext().Authentication;
                authContext.SignIn(identidad);

                // redireccionar al dashboard
                Response.Redirect("~/Pages/Dashboard");
            }
        }
    }
}