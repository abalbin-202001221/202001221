﻿using NetDeveloper.WebForms.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetDeveloper.WebForms.Layout
{
    public partial class Main : MasterPageAuth
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            VerifyAuth();
            // si llega ejecutar las siguientes lineas, significa que el usuario esta autenticado
            if (!IsPostBack)
            {
                LoadUserData();
            }
        }

        protected void LoadUserData()
        {
            // obtener la identidad basada en claims
            var identity = HttpContext.Current.User.Identity as ClaimsIdentity;

            // convertir el identity en una lista de claims
            var claimsList = identity.Claims.ToList();

            // obtener el claim para nombre y para DNI
            var name = claimsList.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value;
            var dni = claimsList.FirstOrDefault(c => c.Type == "DNI").Value;
            var imageClaim = claimsList.FirstOrDefault(c => c.Type == "ImageProfile");
            var imageProfileUrl = imageClaim != null ? imageClaim.Value : "";

            // mostrar los valores en la barra
            pUserInfo.InnerHtml = $"{name} - {dni}";
            imgProfile.ImageUrl = imageProfileUrl;
        }

        protected void lnkCerarSesion_Click(object sender, EventArgs e)
        {
            // obtener el contexto de autenticacion
            var authContext = Request.GetOwinContext().Authentication;

            // cerrar sesion
            authContext.SignOut("ApplicationCookie");

            Response.StatusCode = 401;
        }
    }
}