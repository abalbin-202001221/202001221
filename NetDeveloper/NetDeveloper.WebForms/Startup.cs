﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;
using System.Configuration;

[assembly: OwinStartup(typeof(NetDeveloper.WebForms.Startup))]

namespace NetDeveloper.WebForms
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }

        private void ConfigureAuth(IAppBuilder app)
        {
            // declarar la configuracion de la autenticacion por cookies
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "ApplicationCookie",
                CookieName = "CibertecAuthCookie",
                //ExpireTimeSpan = TimeSpan.FromSeconds(5),
                LoginPath = new PathString("/Account/Login")
            });

            // configurar el aplicativo para que soporte el inicio de sesion con cookies externas (en este caso con Google)
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // configurar el inicio de sesion con google
            app.UseGoogleAuthentication(new Microsoft.Owin.Security.Google.GoogleOAuth2AuthenticationOptions
            {
                ClientId = ConfigurationManager.AppSettings["GoogleClientId"].ToString(),
                ClientSecret = ConfigurationManager.AppSettings["GoogleSecretKey"].ToString()
            });
        }
    }
}
