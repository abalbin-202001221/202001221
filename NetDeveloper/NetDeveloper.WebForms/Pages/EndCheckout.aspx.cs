﻿using NetDeveloper.WebForms.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetDeveloper.WebForms.Pages
{
    public partial class EndCheckout : BasePageAuth
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // obtener el nuevo id de sesion
                if(Session["newInvoiceId"] != null && int.TryParse(Session["newInvoiceId"].ToString(), out int invoiceId))
                {
                    litOrder.Text = invoiceId.ToString();
                }
            }
        }
    }
}