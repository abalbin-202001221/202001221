﻿using NetDeveloper.DataAccess;
using NetDeveloper.WebForms.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

namespace NetDeveloper.WebForms.Pages
{
    public partial class Dashboard : BasePageAuth
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // verificar si esta auntenticado, si no lo esta redireccionar al login
            VerifyAuth();
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        protected void LoadData()
        {
            var xCategoriesArray = new List<string>();
            var dataSerieAlbumArray = new List<int>();
            using (var uow = new UnitOfWork(chinookContext))
            {
                // obtener el reporte del repositorio y tomar los 10 primeros
                var albumsArtists = uow.Artists.ObtenerCantidadDeAlbumesPorArtista().OrderByDescending(r => r.AlbumCount).Take(10).ToList();
                xCategoriesArray = albumsArtists.Select(r => r.ArtistName).ToList();
                dataSerieAlbumArray = albumsArtists.Select(r => r.AlbumCount).ToList();
            }

            // "parsear" los arreglos a JSON
            var xCategoriesJSON = JsonConvert.SerializeObject(xCategoriesArray); // --> ['a','b']
            var dataSerieAlbumJSON = JsonConvert.SerializeObject(dataSerieAlbumArray); // --> [1,2]

            // armar el valor textual del literal
            litVariablesChart.Text = $@"<script type=""text/javascript"">
                                                var xCategories = {xCategoriesJSON};
                                                var dataSerieAlbumes = {dataSerieAlbumJSON};
                                        </script>";

        }
    }
}