﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout/Main.Master" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="NetDeveloper.WebForms.Pages.Customer.List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <h1>Lista de Clientes</h1>
    <asp:UpdatePanel runat="server" ID="upnlGrid" UpdateMode="Always">
        <ContentTemplate>
            <asp:Button runat="server" ID="btnNuevo" Text="Registrar Cliente" OnClick="btnNuevo_Click" CssClass="btn btn-primary btn-sm mb-2" Visible="false" />
            <asp:GridView runat="server" ID="gvCustomers" CssClass="table table-striped" GridLines="None"
                AutoGenerateColumns="false" AllowPaging="true" PageSize="5" PagerSettings-Mode="NumericFirstLast"
                OnPageIndexChanging="gvCustomers_PageIndexChanging" OnRowCommand="gvCustomers_RowCommand">
                <Columns>
                    <asp:BoundField DataField="CustomerId" HeaderText="ID" />
                    <%--<asp:BoundField DataField="FirstName" HeaderText="Nombres" />
            <asp:BoundField DataField="LastName" HeaderText="Apellidos" />--%>
                    <asp:TemplateField HeaderText="Nombres">
                        <ItemTemplate>
                            <span><i class="far fa-address-card"></i><%# Eval("FirstName") %> <%# Eval("LastName") %></span>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField DataField="PostalCode" HeaderText="Cod. Postal" ItemStyle-HorizontalAlign="Right" />
                    <%-- <asp:HyperLinkField DataTextField="Email" HeaderText="Correo" NavigateUrl="https://aaaa" />--%>
                    <asp:TemplateField HeaderText="Email">
                        <ItemTemplate>
                            <a href="mailto:<%# Eval("Email") %>"><i class="far fa-envelope"></i><%# Eval("Email") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Telefono">
                        <ItemTemplate>
                            <a href="tel:<%# Eval("Phone") %>"><%# Eval("Phone") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ImageField DataImageUrlField="ImageProfile"></asp:ImageField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton runat="server" CssClass="btn btn-link btn-sm" CommandName="edit-record" CommandArgument='<%# Eval("CustomerId") %>'><i class="fas fa-edit"></i>Editar</asp:LinkButton>
                            <asp:LinkButton runat="server" CssClass="btn btn-link btn-sm" CommandName="delete-record" CommandArgument='<%# Eval("CustomerId") %>' OnClientClick="javascript:return mostrarAlerta()"><i class="fas fa-trash"></i>Eliminar</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:Panel runat="server" ID="pnlEditar" Visible="false">
                

                
                <%--<asp:Button runat="server" ID="btnGuardar" Text="Guardar" OnClick="btnGuardar_Click" CssClass="btn btn-secondary btn-sm" Visible="false" />--%>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--Inicio de Modal--%>
    <div class="modal fade" id="modalCustomer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel runat="server" ID="upModal">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="titleModal" runat="server"><asp:Literal runat="server" ID="litTitle"></asp:Literal></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="txtNombres">Nombres</label>
                                    <asp:TextBox runat="server" ID="txtNombres" CssClass="form-control" ClientIDMode="Static" placeholder="Ingresa un nombre" autocomplete="disable"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="txtApellidos">Apellidos</label>
                                    <asp:TextBox runat="server" ID="txtApellidos" CssClass="form-control" ClientIDMode="Static" placeholder="Ingresa los apellidos" autocomplete="disable"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="txtEmail">Email</label>
                                    <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" ClientIDMode="Static" placeholder="Ingresa el email" autocomplete="disable"></asp:TextBox>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="txtTelefono">Telefono</label>
                                    <asp:TextBox runat="server" ID="txtTelefono" CssClass="form-control" ClientIDMode="Static" placeholder="Ingresa un telefono" autocomplete="disable"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-12">
                                    <label for="ddlSupportRep">Representante de Soporte</label>
                                    <asp:DropDownList runat="server" ID="ddlSupportRep" CssClass="form-control" ClientIDMode="Static" DataTextField="FirstName" DataValueField="EmployeeId" Width="100%"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-12">
                                    <label for="txtEmail">Fecha de Nacimiento</label>
                                    <asp:TextBox runat="server" ID="txtFechaNacimiento" CssClass="form-control" ClientIDMode="Static" placeholder="Ingresa la fecha de nacimiento" autocomplete="disable" TextMode="DateTimeLocal"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <asp:HiddenField runat="server" ID="hdnCustomerId" />
                            <asp:Button runat="server" ID="btnGuardar" CssClass="btn btn-primary" OnClick="btnGuardar_Click" Text="Guardar" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </div>
    <%--Fin del modal--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBottomScripts" runat="server">
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        function inicializarSelect2() {
            // inicializar el select2
            $("#ddlSupportRep").select2();
            $("#txtFechaNacimiento").flatpickr();
        }
        function showModal() {
            $("#modalCustomer").modal({ show: true });
        }
        function closeModal() {
            $("#modalCustomer").modal("hide");
        }
        function mostrarAlerta() {
            return confirm("Estas seguro de eliminar este registro?");
        }
        function loadDatePicker() {
            
        }
    </script>
</asp:Content>
