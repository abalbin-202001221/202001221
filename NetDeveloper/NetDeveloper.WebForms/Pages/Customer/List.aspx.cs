﻿using NetDeveloper.DataAccess;
using NetDeveloper.DataAccess.EF;
using NetDeveloper.WebForms.App_Code;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetDeveloper.WebForms.Pages.Customer
{
    public partial class List : BasePageAuth
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Que es un postback?
            // es un evento que se lanza cada vez que recarga la pagina, ya sea porque se esta cambiando de pagina de grilla o presionando un boton
            // o ejecutando cualquier accion que requiera que se refresque la pagina actual
            if (!IsPostBack)
            {
                LoadControls();
                LoadData();
            }
        }

        protected void LoadControls()
        {
            // si el usuario tiene el rol admin o editor, vamos a mostrar los botones para guardar o para insertar un nuevo customer
            if (IsInRole("admin") || IsInRole("editor"))
            {
                btnNuevo.Visible = true;
                btnGuardar.Visible = true;
            }
        }


        protected void LoadData(int pageIndex = 0)
        {
            gvCustomers.PageIndex = pageIndex;
            // cargar la data de la grilla de customers
            using (var uow = new UnitOfWork(chinookContext))
            {
                gvCustomers.DataSource = uow.Customers.ObtenerTodos().ToList();
            }
            // este comando es importante para enlazar la fuente de datos con el control asp
            gvCustomers.DataBind();
        }

        protected void gvCustomers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            // establecer el nuevo indice de la pagina y volver a cargar la data           
            LoadData(e.NewPageIndex);
        }

        protected void gvCustomers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            // Hacer un switch para poder realizar logica diferente por cada tipo de comando
            switch (e.CommandName)
            {
                case "edit-record":
                    {
                        if (int.TryParse(e.CommandArgument.ToString(), out int customerId))
                        {
                            // mostrar el panel de edicion
                            pnlEditar.Visible = true;
                            // definir el valor del titulo
                            litTitle.Text = "Editar un cliente";
                            // cargar la data en el formulario
                            LoadEditForm(customerId);
                            // inicializar select2
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "initSelect2", "inicializarSelect2()", true);
                            // mostrar el modal
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "openModal", "showModal()", true);
                        }

                        break;
                    }
                case "delete-record":
                    {
                        if (int.TryParse(e.CommandArgument.ToString(), out int customerId))
                        {
                            using (var uow = new UnitOfWork(chinookContext))
                            {
                                uow.Customers.Eliminar(customerId);
                                uow.Complete();
                                LimpiarFormulario();
                                pnlEditar.Visible = false;
                                LoadData();
                            }
                        }
                        break;
                    }
                case "Page":
                    {
                        pnlEditar.Visible = false;
                        break;
                    }
                default: break;
            }
        }

        protected void LoadEditForm(int customerId)
        {
            var customer = new Models.Customer();

            // obtener la data del registro de la BD
            using (var uow = new UnitOfWork(chinookContext))
            {
                customer = uow.Customers.ObtenerPorId(customerId);
            }

            // cargar la data a los controles del formulario
            txtApellidos.Text = customer.LastName;
            txtNombres.Text = customer.FirstName;
            txtEmail.Text = customer.Email;
            txtTelefono.Text = customer.Phone;
            hdnCustomerId.Value = customer.CustomerId.ToString();
            CargarDataRepresentantes(customer.SupportRepId.HasValue ? customer.SupportRepId.ToString() : null);
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            // obtener el customerId del campo hidden
            var customerIdStr = hdnCustomerId.Value;

            // hacer el aprse a entero y si es verdadero, continuar con el algoritmo
            if (int.TryParse(customerIdStr, out int customerId))
            {
                var resultadoGuardar = 0;
                using (var uow = new UnitOfWork(chinookContext))
                {
                    // obtener el registro actual de la BD
                    // si el customerId es 0, significa que queremos insertar
                    // en el caso de insertar, customer debera ser una nueva instancia de la clase
                    var customer = customerId == 0 ? new Models.Customer() : uow.Customers.ObtenerPorId(customerId);

                    if (customer == null)
                    {
                        return;
                    }

                    // asignar sus campos
                    customer.LastName = txtApellidos.Text.Trim();
                    customer.FirstName = txtNombres.Text.Trim();
                    customer.Email = txtEmail.Text.Trim();
                    customer.Phone = txtTelefono.Text.Trim();
                    var cultureUS = new CultureInfo("en-US");
                    if (DateTime.TryParseExact(txtFechaNacimiento.Text, "yyyy-MM-dd", cultureUS, DateTimeStyles.None, out DateTime fechaNac))
                    {
                        customer.BirthDate = fechaNac;
                    }
                    if (int.TryParse(ddlSupportRep.SelectedValue, out int supportRepId))
                    {
                        customer.SupportRepId = supportRepId;
                    }

                    if (customerId == 0)
                    {
                        uow.Customers.Insertar(customer);
                    }

                    // al actualizar los campos, lo que hace EF es verificar si existen cambios en la entidad y si los hay,
                    // se va a realizar un update de la entidad en la BD
                    resultadoGuardar = uow.Complete();
                }

                // si el valor de resultadoGuardar es mayor 0, significa que se edito/inserto de forma satsifactoria
                if (resultadoGuardar > 0)
                {
                    pnlEditar.Visible = false;
                    // cerrar el modal
                    // mostrar el modal
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "closeModal", "closeModal()", true);
                    // actualizar la lista
                    LoadData();
                }
            }
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            // mostrar el panel
            pnlEditar.Visible = true;
            // definir el valor del titulo
            litTitle.Text = "Insertar un cliente";
            // definir el valor del campo escondido
            hdnCustomerId.Value = "0";
            LimpiarFormulario();
            CargarDataRepresentantes();

            // inicializar select2
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "initSelect2", "inicializarSelect2()", true);
            // mostrar el modal
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "openModal", "showModal()", true);
        }

        protected void LimpiarFormulario()
        {
            txtApellidos.Text = "";
            txtEmail.Text = "";
            txtNombres.Text = "";
            txtTelefono.Text = "";
        }

        protected void CargarDataRepresentantes(string selectedValue = null)
        {
            using (var uow = new UnitOfWork(chinookContext))
            {
                ddlSupportRep.DataSource = uow.Employees.ObtenerTodos().ToList();
                ddlSupportRep.DataBind();
            }

            // agregar un elemento comodin como primer elemento del drop down
            ddlSupportRep.Items.Insert(0, new ListItem { Text = "Selecciona...", Value = null });

            // establecer el elemento a ser seleccionado
            ddlSupportRep.SelectedValue = selectedValue;
        }
    }
}