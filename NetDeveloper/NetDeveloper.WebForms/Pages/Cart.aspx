﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Cart.aspx.cs" Inherits="NetDeveloper.WebForms.Pages.Cart" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/01b3dd551d.js" crossorigin="anonymous"></script>
    <title>Ver Carrito de compras</title>
</head>
<body>
    <form id="form1" runat="server">
        <h1>Carrito de Compras</h1>
        <asp:Repeater runat="server" ID="rptCartItems" ItemType="NetDeveloper.Models.ViewModels.CartItemViewModel">
            <HeaderTemplate>
                <ul class="list-unstyled">
            </HeaderTemplate>
            <ItemTemplate>
                <asp:HiddenField runat="server" ID="hdnItemId" Value="<%# Item.TrackId %>" />
                <asp:HiddenField runat="server" ID="hdnNombre" Value="<%# Item.Nombre %>" />
                <asp:HiddenField runat="server" ID="hdnPrecio" Value="<%# Item.Price %>" />
                <p>Nombre: <%# Item.Nombre %></p>
                <p>Precio: S/ <%# Item.Price %></p>
                <p>Cantidad: <asp:TextBox runat="server" ID="txtCantidad" Text="<%# Item.Quantity %>"></asp:TextBox></p>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>           
        </asp:Repeater>
        <asp:Button runat="server" ID="btnComprar" Text="Comprar" CssClass="btn btn-primary" OnClick="btnComprar_Click" />
    </form>
</body>
</html>
