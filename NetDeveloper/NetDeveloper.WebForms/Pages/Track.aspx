﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Track.aspx.cs" Inherits="NetDeveloper.WebForms.Pages.Track" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/01b3dd551d.js" crossorigin="anonymous"></script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand h1" href="#">Tienda Virtual</a>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                <asp:Button runat="server" CssClass="btn btn-outline-success my-2 my-sm-0" Text="Ver Carrito" ID="btnVerCarrito" OnClick="btnVerCarrito_Click" />
            </div>
        </nav>
        <asp:ScriptManager runat="server"></asp:ScriptManager>
        <h1>Listado de canciones</h1>
        <p class="lead">
            <asp:Label runat="server" ID="lblAlbumId"></asp:Label>
        </p>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Repeater runat="server" ID="rptTracks" ItemType="NetDeveloper.Models.Track" OnItemCommand="rptTracks_ItemCommand">
                    <HeaderTemplate>
                        <div style="display: flex; flex-wrap: wrap; justify-content: space-around;">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="card" style="width: 18rem; margin-bottom: 1em;">
                            <img src="https://place-hold.it/300" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title"><%# Item.Name %></h5>
                                <p><i class="far fa-clock"></i><%# Item.Milliseconds %></p>
                                <p><i class="fas fa-dollar-sign"></i><%# Item.UnitPrice %></p>
                                <asp:LinkButton runat="server" ID="lnkAddToCart" CssClass="btn btn-primary btn-sm" CommandArgument="<%# Item.TrackId %>" CommandName="AddToCart">Agregar al carrito</asp:LinkButton>
                            </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
            </ContentTemplate>
        </asp:UpdatePanel>

    </form>
</body>
</html>
