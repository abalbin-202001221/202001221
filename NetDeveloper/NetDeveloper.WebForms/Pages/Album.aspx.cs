﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetDeveloper.DataAccess.EF;
using NetDeveloper.DataAccess;
using System.Configuration;

namespace NetDeveloper.WebForms.Pages
{
    public class ClasePrueba
    {
        public string Name { get; set; }
        public int Value { get; set; }  
    }

    public partial class Album : System.Web.UI.Page
    {
        protected readonly ChinookContext _context;
        public Album()
        {
            _context = new ChinookContext(ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        protected void LoadData()
        {
            // asignar la data al repeater de albumes
            //rptAlbums.DataSource = new string[] { "uno", "dos", "tres" };
            //rptAlbums.DataSource = new ClasePrueba[] {
            //    new ClasePrueba { Name="Item 1", Value=123 },
            //    new ClasePrueba { Name = "Item 2", Value = 1234 },
            //    new ClasePrueba { Name = "Item 2", Value = 12345 }
            //};

            // utilizando el Unit Of Work, que se inicializa con un contexto de BD existente, 
            // vamos a obtener la lista de artistat que existe en la tabla de nuestra BD
            using (var uow = new UnitOfWork(_context))
            {
                rptAlbums.DataSource = uow.Albums.ObtenerTodos().ToList();
                rptAlbums.DataBind();
            }
        }
    }
}