﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Checkout.aspx.cs" Inherits="NetDeveloper.WebForms.Pages.Checkout" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/01b3dd551d.js" crossorigin="anonymous"></script>
    <title>Realiza el pago</title>
</head>
<body>
    <form id="form1" runat="server">
        <h1>Finaliza la compra</h1>
        <asp:Repeater runat="server" ID="rptCartItems" ItemType="NetDeveloper.Models.ViewModels.CartItemViewModel">
            <HeaderTemplate>
                <ul class="list-unstyled">
            </HeaderTemplate>
            <ItemTemplate>
                <p>Nombre: <%# Item.Nombre %></p>
                <p>Precio: S/ <%# Item.Price %></p>
                <p>Cantidad: <%# Item.Quantity %></p>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>           
        </asp:Repeater>
        <p>
            <strong>Total:</strong> <asp:Literal runat="server" ID="litTotal"></asp:Literal>
        </p>
        <asp:Button runat="server" ID="btnFinalizar" CssClass="btn btn-primary" Text="Finalizar" OnClick="btnFinalizar_Click" />
    </form>
</body>
</html>
