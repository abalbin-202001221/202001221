﻿using NetDeveloper.DataAccess;
using NetDeveloper.Models;
using NetDeveloper.Models.ViewModels;
using NetDeveloper.WebForms.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetDeveloper.WebForms.Pages
{
    public partial class Checkout : BasePageAuth
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // verificar si el usuario esta autenticado, si no lo esta pedir que se autentique
            VerifyAuth();
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        protected void LoadData()
        {
            // obtener el objeto de sesion
            var cartItemsSession = Session["cartItemsSession"] as List<CartItemViewModel>;
            if(cartItemsSession == null)
            {
                // Redireccionar al carrito
                Response.Redirect("~/Pages/Cart");
            }

            // calcular el total
            litTotal.Text = cartItemsSession.Sum(c => c.Price * c.Quantity).ToString();

            rptCartItems.DataSource = cartItemsSession;
            rptCartItems.DataBind();
        }

        protected void btnFinalizar_Click(object sender, EventArgs e)
        {
            // guardar invoice e invoice lines en BD
            // obtener el objeto de sesion
            var cartItemsSession = Session["cartItemsSession"] as List<CartItemViewModel>;
            if (cartItemsSession == null)
            {
                // Redireccionar al carrito
                Response.Redirect("~/Pages/Cart");
            }

            // obtener el customer id de los claims de la cookie de auth
            var identity = HttpContext.Current.User.Identity as ClaimsIdentity;
            var claimsList = identity.Claims.ToList();
            var customerIdClaim = claimsList.FirstOrDefault(c => c.Type == "CustomerId");

            if(customerIdClaim == null || !int.TryParse(customerIdClaim.Value, out int customerId))
            {
                throw new Exception("El usuario no cuenta con un id de cliente valido");
            }

            // crear el invoice
            var invoice = new Invoice
            {
                CustomerId = customerId,
                InvoiceDate = DateTime.Now,
                Total = cartItemsSession.Sum(c => c.Price * c.Quantity),
                InvoiceLines = cartItemsSession.Select(c => new InvoiceLine { Quantity = c.Quantity, TrackId = c.TrackId, UnitPrice = c.Price }).ToList()
            };

            var resultadoOrden = 0;
            using(var uow = new UnitOfWork(chinookContext))
            {
                uow.Invoices.Insertar(invoice);
                resultadoOrden = uow.Complete();
            }

            if(resultadoOrden > 0)
            {
                // significa que se registro la compra
                // borrar el carrito de sesion
                Session.Remove("cartItemsSession");
                // borrar la cookie del carrito
                var cookie = Request.Cookies.Get("cartCookie");
                cookie.Expires = DateTime.Now.AddDays(-1);
                Response.SetCookie(cookie);
                // guardar en una variable de sesion el id de la nueva orden
                Session["newInvoiceId"] = invoice.InvoiceId;
                // redireccionar al end checkout
                Response.Redirect("~/Pages/EndCheckout");
            }
        }
    }
}