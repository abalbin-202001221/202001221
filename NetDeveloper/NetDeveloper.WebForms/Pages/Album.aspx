﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Album.aspx.cs" Inherits="NetDeveloper.WebForms.Pages.Album" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/01b3dd551d.js" crossorigin="anonymous"></script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <h1>Albums</h1>
        <asp:Repeater runat="server" ID="rptAlbums" ItemType="NetDeveloper.Models.Album">
            <HeaderTemplate>
                <table class="table table-dark">
                    <thead>
                        <tr>
                            <th>
                                Title
                            </th>
                            <th>

                            </th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <%--El unico elemento que se repite es el item template--%>
            <ItemTemplate>
                <tr>
                    <td><%# Item.Title %></td>
                    <td><a href="Track?albumId=<%# Item.AlbumId %>" target="_blank"><i class="fas fa-music"></i> Ver Canciones</a></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </form>
</body>
</html>
