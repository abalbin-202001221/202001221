﻿using NetDeveloper.DataAccess;
using NetDeveloper.DataAccess.EF;
using NetDeveloper.WebForms.App_Code;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetDeveloper.WebForms.Pages
{
    public partial class Track : StoreBasePage
    {
        protected readonly ChinookContext _context;
        public Track()
        {
            _context = new ChinookContext(ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        protected void LoadData()
        {
            // leer los params del query string
            var albumId = Request.QueryString["albumId"];

            // si el album id no existe o esta vacio, detener la ejecucion del metodo
            if (string.IsNullOrEmpty(albumId))
            {              
                return;
            }
         
            // si el album id no es un numero, detener la ejecucion
            if (!int.TryParse(albumId, out int intAlbumId))
            {
                return;
            }

            // obtener los tracks del album solicitado
            using(var uow = new UnitOfWork(_context))
            {
                var tracks = uow.Tracks.GetByAlbumId(intAlbumId).ToList();
                lblAlbumId.Text = $"Album ID: {albumId} - Cantidad de canciones: {tracks.Count}";
                rptTracks.DataSource = tracks;
                rptTracks.DataBind();
            }
        }       

        protected void rptTracks_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if(e.CommandName == "AddToCart")
            {
                var trackId = e.CommandArgument;
                // obtener la cookie
                var cartCookie = CartCookie;
                if(cartCookie == null)
                {
                    // significa que la cookie no existe, entonces la vamos a crear
                    var newCookie = new HttpCookie("cartCookie");
                    newCookie.Value = trackId.ToString();
                    Response.Cookies.Add(newCookie);
                }
                else
                {
                    cartCookie.Value = $"{cartCookie.Value},{trackId}";
                    Response.SetCookie(cartCookie);
                }
            }
            
        }

        protected void btnVerCarrito_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Pages/Cart");
        }
    }
}