﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout/Main.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="NetDeveloper.WebForms.Pages.Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <h1>Dashboard</h1>
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Top 10 de Artistas con mayor cantidad de albumes</h5>
            <div id="container" style="width: 100%; height: 400px;"></div>
        </div>
    </div>
    <asp:Literal runat="server" ID="litVariablesChart"></asp:Literal>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBottomScripts" runat="server">
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {
            var myChart = Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Top 10 de Artistas con mayor cantidad de albumes'
                },
                subtitle: {
                    text: "Algun subtitulo"
                },
                xAxis: {
                    categories: xCategories
                },
                yAxis: {
                    title: {
                        text: 'Cantidad de albumes'
                    }
                },
                series: [{
                    name: 'Cantidad de Albumes',
                    data: dataSerieAlbumes
                }]
            });
        });
    </script>
</asp:Content>
