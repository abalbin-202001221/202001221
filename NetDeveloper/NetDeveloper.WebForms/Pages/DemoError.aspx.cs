﻿using NetDeveloper.WebForms.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetDeveloper.WebForms.Pages
{
    public partial class DemoError : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var logger = log4net.LogManager.GetLogger(GetType());
            logger.Info(new { Message = "El usuario ha visitado esta pagina que no deberia", Request.UserHostAddress, Request.UserAgent, Request.QueryString });
            // generar un error a proposito
            //throw new Exception("Este es un error generado");
        }

        protected void btnError_Click(object sender, EventArgs e)
        {
            var logger = log4net.LogManager.GetLogger(GetType());
            logger.Warn(new { Message = "El usuario ha visitado esta pagina que no deberia", Request.UserHostAddress, Request.UserAgent });
        }
    }
}