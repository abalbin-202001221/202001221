﻿using NetDeveloper.DataAccess;
using NetDeveloper.WebForms.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetDeveloper.WebForms.Pages.Artist
{
    public partial class List : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        protected void LoadData()
        {
            // cargar la data de artistas y mostrarla en el repeater
            using(var uow = new UnitOfWork(chinookContext))
            {
                rptArtists.DataSource = uow.Artists.ObtenerTodos().ToList();
                rptArtists.DataBind();
            }
        }
    }
}