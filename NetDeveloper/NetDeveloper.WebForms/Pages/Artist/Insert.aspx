﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Insert.aspx.cs" Inherits="NetDeveloper.WebForms.Pages.Artist.Insert" MasterPageFile="~/Layout/Main.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="cphMain">
    <h1>Insertar un Artista</h1>
    <asp:Panel runat="server" CssClass="alert alert-success alert-dismissible fade show" role="alert" ID="pnlAlerta" Visible="false">
        Se registro el artista satisfactoriamente
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
    </asp:Panel>
    <div class="form-group">
        <label for="txtName">Name</label>
        <asp:TextBox runat="server" ID="txtName" CssClass="form-control"></asp:TextBox>
        <div class="invalid-feedback">
            Por favor, ingresa un valor
        </div>
        <small id="helpName" class="form-text text-muted">En este campo puedes ingresar el nombre del artista nuevo que deseas crear.</small>
        <%--<asp:RequiredFieldValidator runat="server" EnableClientScript="true" ControlToValidate="txtName" Display="Dynamic">
                <small class="text-danger">
                    Este valor es obligatorio
                </small>
            </asp:RequiredFieldValidator>--%>
    </div>
    <asp:Button runat="server" ID="btnSave" CssClass="btn btn-primary" Text="Guardar" OnClick="btnSave_Click" />
    <asp:LinkButton runat="server" ID="lnkSave" CssClass="btn btn-primary" OnClick="btnSave_Click">
            <i class="fas fa-save"></i> Guardar
    </asp:LinkButton>
    <asp:LinkButton runat="server" ID="lnkSaveSP" CssClass="btn btn-primary" OnClick="lnkSaveSP_Click">
            <i class="fas fa-save"></i> Guardar con SP
    </asp:LinkButton>
</asp:Content>

