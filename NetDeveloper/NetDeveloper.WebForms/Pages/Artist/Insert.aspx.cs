﻿using NetDeveloper.DataAccess;
using NetDeveloper.DataAccess.EF;
using NetDeveloper.WebForms.App_Code;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetDeveloper.WebForms.Pages.Artist
{
    public partial class Insert : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            // Obtener los valores ingresados por el usuario
            // El atributo Text de un control TextBox obtiene lo que el usuario ingreso en la caja de texto
            var name = txtName.Text;
            // Validaciones
            if (string.IsNullOrEmpty(name))
            {
                // si el nombre es vacio
                txtName.CssClass = "form-control is-invalid";
                return;
            }

            // Insertar un nuevo artista en BD
            var resultInsert = 0;
            using (var uow = new UnitOfWork(chinookContext))
            {
                var nuevoArtista = new Models.Artist { Name = name };
                uow.Artists.Insertar(nuevoArtista);
                resultInsert = uow.Complete();
            }

            // validar si la insercion fue correcta
            if(resultInsert > 0)
            {
                txtName.Text = string.Empty;
                pnlAlerta.Visible = true;
                txtName.CssClass = "form-control";
            }
        }

        protected void lnkSaveSP_Click(object sender, EventArgs e)
        {
            // Obtener los valores ingresados por el usuario
            // El atributo Text de un control TextBox obtiene lo que el usuario ingreso en la caja de texto
            var name = txtName.Text;
            // Validaciones
            if (string.IsNullOrEmpty(name))
            {
                // si el nombre es vacio
                return;
            }

            // Insertar un nuevo artista en BD
            var resultInsert = 0;
            using (var uow = new UnitOfWork(chinookContext))
            {
                var nuevoArtista = new Models.Artist { Name = name };
                resultInsert = uow.Artists.InsertWithSP(nuevoArtista);
            }

            // validar si la insercion fue correcta
            if (resultInsert > 0)
            {
                txtName.Text = string.Empty;
                pnlAlerta.Visible = true;
            }
        }
    }
}