﻿using NetDeveloper.DataAccess;
using NetDeveloper.DataAccess.EF;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetDeveloper.WebForms.Pages
{
    public partial class Demo : System.Web.UI.Page
    {
        private readonly ChinookContext _context;

        public Demo()
        {
            _context = new ChinookContext(ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString);
        }
        /// <summary>
        /// Esta funcion se ejecuta en el evento mediante el cual se carga la pagina
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            using(var uow = new UnitOfWork(_context))
            {
                var artistCount = uow.Artists.ObtenerTodos().Count();
                lblNombre.Text = $"Valor asignado desde el servidor - Cantidad de elementos en la tabla artista: {artistCount}";
            }
            
        }
    }
}