﻿using NetDeveloper.DataAccess;
using NetDeveloper.Models.ViewModels;
using NetDeveloper.WebForms.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetDeveloper.WebForms.Pages
{
    public partial class Cart : StoreBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadCartItems();
            }
        }

        protected void LoadCartItems()
        {
            // obtener la cookie
            var cookie = CartCookie;

            if (cookie == null)
            {
                // eventualmente habria que mostrar un mensaje en la pagina que diga que el usuario aun no hay agregado items a su carrito
                return;
            }

            // armar un diccionario de los items (para contar cuantos items existen para un mismo id)
            var itemDictionary = new Dictionary<string, int>();
            // obtener el valor de la cookie como si fuera un arreglo de string
            // Ejemplo: 1,2,3 -> ["1","2","3","1"]
            var itemIdArray = cookie.Value.Split(',');
            foreach (var itemId in itemIdArray)
            {
                if (itemDictionary.TryGetValue(itemId, out int curentQuantity))
                {
                    itemDictionary[itemId] += 1;
                }
                else
                {
                    itemDictionary[itemId] = 1;
                }
            }

            // declarar la lista de cartItems
            var cartItems = new List<CartItemViewModel>();

            // recorrer el diccionario para obtener la informacion de cada item
            using (var uow = new UnitOfWork(chinookContext))
            {
                foreach (var item in itemDictionary)
                {
                    // obtener la informacion de la cancion
                    var track = uow.Tracks.ObtenerPorId(Convert.ToInt32(item.Key));
                    if (track == null)
                    {
                        continue;
                    }
                    cartItems.Add(new CartItemViewModel { Price = track.UnitPrice, Nombre = track.Name, Quantity = item.Value, TrackId = track.TrackId });
                }
            }

            // enlazar la lista con el repeater
            rptCartItems.DataSource = cartItems;
            rptCartItems.DataBind();
        }

        protected void btnComprar_Click(object sender, EventArgs e)
        {
            var items = rptCartItems.Items;
            var cartItemsSession = new List<CartItemViewModel>();

            foreach (RepeaterItem item in items)
            {
                // obtener los valores
                var trackId = (item.FindControl("hdnItemId") as HiddenField).Value;
                var nombre = (item.FindControl("hdnNombre") as HiddenField).Value;
                var precio = (item.FindControl("hdnPrecio") as HiddenField).Value;
                var cantidad = (item.FindControl("txtCantidad") as TextBox).Text;

                cartItemsSession.Add(new CartItemViewModel
                {
                    TrackId = Convert.ToInt32(trackId),
                    Nombre = nombre,
                    Price = Convert.ToDecimal(precio),
                    Quantity = Convert.ToInt32(cantidad)
                });
            }

            // grabar la lista en sesion
            Session["cartItemsSession"] = cartItemsSession;
            Response.Redirect("~/Pages/Checkout");
        }
    }
}