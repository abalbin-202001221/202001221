﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EndCheckout.aspx.cs" Inherits="NetDeveloper.WebForms.Pages.EndCheckout" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/01b3dd551d.js" crossorigin="anonymous"></script>
    <title>Post Compra</title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="d-flex align-items-center flex-column p-4">
            <i class="fas fa-shipping-fast fa-10x text-info"></i>
            <h1>Tu compra ha sido registrada
            </h1>
            <h2>Tu numero de orden es: #<asp:Literal runat="server" ID="litOrder"></asp:Literal>. Ante cualquier duda comunicate a <a href="mailto:algun@correo.com">algun@correo.com</a>
            </h2>
        </div>
    </form>
</body>
</html>
