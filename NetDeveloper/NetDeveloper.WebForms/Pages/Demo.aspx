﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Demo.aspx.cs" Inherits="NetDeveloper.WebForms.Pages.Demo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label runat="server" ID="lblNombre"></asp:Label>
        <hr />
        <div class="form-group">
            <label>Contrasena</label>
            <asp:TextBox runat="server" ID="txtPass" TextMode="Password" ValidationGroup="validPass"></asp:TextBox>
        </div>
        <div class="form-group">
            <label>Repetir Contrasena</label>
            <asp:TextBox runat="server" ID="txtRepeatPass" TextMode="Password" ValidationGroup="validPass"></asp:TextBox>
            <asp:CompareValidator runat="server" ControlToValidate="txtRepeatPass" ControlToCompare="txtPass" ErrorMessage="Las contrasenas deben ser iguales" EnableClientScript="false" Display="Dynamic" ValidationGroup="validPass"></asp:CompareValidator>
        </div>
        <asp:Button runat="server" Text="Validar Contrasenas" ValidationGroup="validPass" />
        <hr />
        <div class="form-group">
            <label>Ingresa tu edad</label>
            <asp:TextBox runat="server" ID="txtEdad" ValidationGroup="validEdad"></asp:TextBox>
            <asp:RangeValidator runat="server" EnableClientScript="true" Display="Dynamic" MinimumValue="18" MaximumValue="200" ErrorMessage="Debes ser mayor de edad" ControlToValidate="txtEdad" ValidationGroup="validEdad"></asp:RangeValidator>
        </div>
        <asp:Button runat="server" Text="Validar Edad" ValidationGroup="validEdad" />
    </form>
</body>
</html>
