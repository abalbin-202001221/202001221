﻿using NetDeveloper.DataAccess;
using NetDeveloper.DataAccess.EF;
using NetDeveloper.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace NetDeveloper.WebApi.Controllers
{
    // /api/artist
    public class ArtistController : ApiController
    {
        protected readonly ChinookContext _context;
        public ArtistController()
        {
            // inicializar el contexto
            _context = new ChinookContext(ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString);
        }

        // GET /api/artist/
        [HttpGet]
        [Route("api/artistas/obtener")]
        [Route("api/artist")]
        public IEnumerable<Artist> Get()
        {
            using(var uow = new UnitOfWork(_context))
            {
                return uow.Artists.ObtenerTodos().ToList();
            }
        }
    }
}