﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace NetDeveloper.WebApi.Controllers
{
    public class ObjetoGet
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
    public class DemoController : ApiController
    {
        public ObjetoGet[] Get()
        {
            return new ObjetoGet[] { new ObjetoGet { Id = 1, Name = "Objeto 1" }, new ObjetoGet { Id = 2, Name = "Objeto 2" } };
        }

        [HttpGet]
        [Route("api/demo/ejemplo1")]
        public string Ejemplo1()
        {
            return "ejemplo 1";
        }

        [HttpGet]
        [Route("api/demo/ejemplo2/{nombre}/{edad}")]
        public string Ejemplo2(string nombre, int edad)
        {
            return $"{nombre} - Edad: {edad}";
        }
    }
}