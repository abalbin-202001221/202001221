﻿using NetDeveloper.DataAccess;
using NetDeveloper.DataAccess.EF;
using NetDeveloper.Models.Reports;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace NetDeveloper.WebApi.Controllers
{
    public class SalesController : ApiController
    {
        private readonly ChinookContext _context;
        public SalesController()
        {
            _context = new ChinookContext(ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString);
        }

        [HttpGet]
        [Route("api/sales/track-report")]
        public IEnumerable<TrackSalesReport> GetTrackReport()
        {
            using (var unit = new UnitOfWork(_context))
            {
                return unit.Tracks.GetTrackSalesReport();
            }
        }
    }
}