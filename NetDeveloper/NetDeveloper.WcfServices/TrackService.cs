﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using NetDeveloper.DataAccess;
using NetDeveloper.DataAccess.EF;
using NetDeveloper.Models.DataContracts;

namespace NetDeveloper.WcfServices
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "TrackService" en el código y en el archivo de configuración a la vez.
    public class TrackService : ITrackService
    {
        private readonly ChinookContext _context;
        public TrackService()
        {
            _context = new ChinookContext(ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString);
        }
        public IEnumerable<TrackSalesDataContract> GetSalesReport()
        {
            using (var unit = new UnitOfWork(_context))
            {
                return unit.Tracks.GetTrackSalesReport()
                    .Select(e => new TrackSalesDataContract
                    {
                        TrackId = e.TrackId,
                        Name = e.Name,
                        SalesCount = e.SalesCount
                    });
            }
        }
    }
}
