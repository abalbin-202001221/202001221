﻿using System;
using System.Collections.Generic;
using NetDeveloper.Models.DataContracts;
using NetDeveloper.DataAccess;
using NetDeveloper.DataAccess.EF;
using System.Configuration;
using System.Linq;
using NetDeveloper.Models;

namespace NetDeveloper.WcfServices
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "ArtistService" en el código y en el archivo de configuración a la vez.
    public class ArtistService : IArtistService
    {
        private readonly ChinookContext _context;
        public ArtistService()
        {
            _context = new ChinookContext(ConfigurationManager.ConnectionStrings["ChinookCS"].ConnectionString);
        }

        public int Insertar(ArtistDataContract artist)
        {
            using (var unit = new UnitOfWork(_context))
            {
                var newArtist = new Artist { Name = artist.Name };
                unit.Artists.Insertar(newArtist);
                unit.Complete();
                return newArtist.ArtistId;
            }
        }

        public IEnumerable<ArtistDataContract> ObtenerTodo()
        {
            using (var unit = new UnitOfWork(_context))
            {
                var resultado = unit.Artists.ObtenerTodos();
                return resultado
                    .Select(a => new ArtistDataContract { ArtistId = a.ArtistId, Name = a.Name }).ToList();
            }
        }
    }
}
