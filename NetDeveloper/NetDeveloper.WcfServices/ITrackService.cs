﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using NetDeveloper.Models.DataContracts;

namespace NetDeveloper.WcfServices
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "ITrackService" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface ITrackService
    {
        [OperationContract]
        IEnumerable<TrackSalesDataContract> GetSalesReport();
    }
}
