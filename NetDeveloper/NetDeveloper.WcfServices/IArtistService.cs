﻿using NetDeveloper.Models.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NetDeveloper.WcfServices
{
    // NOTA: todos los métodos que queremos exponer en el servicio, deben estar marcados como OperationContract
    [ServiceContract]
    public interface IArtistService
    {
        [OperationContract]
        IEnumerable<ArtistDataContract> ObtenerTodo();
        [OperationContract]
        int Insertar(ArtistDataContract artist);
    }
}
