﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

namespace NetDeveloper.SWAPI
{
    class Person
    {
        public string Name { get; set; }
        public string Height { get; set; }
        public string Mass { get; set; }
        [JsonProperty("skin_color")]
        public string SkinColor { get; set; }
    }

    class PeopleResult
    {
        public int Count { get; set; }
        public string Next { get; set; }
        public string Previous { get; set; }
        public List<Person> Results { get; set; }
    }

    class Program
    {
        const string SWAPI_URL = "https://swapi.co/api/";

        static void Main(string[] args)
        {
            Console.WriteLine($"Cliente para consultar los servicios REST de SWAPI ({SWAPI_URL})");

            // obtener la data
            GetPeople($"{SWAPI_URL}people");

            Console.WriteLine($"Se encontraron {people.Count}  personas");

            // en este punto people ya tiene los 87 registros
            // ejercicios LINQ

            // Ej. 1: encontrar todos los registros donde SkinColor sea grey
            Console.WriteLine("Personas con skin color grey");

            // con query syntax
            var peopleSkinGrey = from person in people
                                 where person.SkinColor == "grey"
                                 orderby person.Name descending
                                 select person;

            // con fluent syntax
            var peopleSkinGreyFluent = people
                .Where(p => p.SkinColor == "grey")
                .OrderBy(p => p.Name);

            PrintPeople(peopleSkinGreyFluent);


            // Ej. 2: agrupar a las personas por Skin Color
            Console.WriteLine("\n\nPersonas agrupadas por skin color");
            var grupos = people
                .GroupBy(p => p.SkinColor);

            foreach(var grupoSkin in grupos)
            {
                Console.WriteLine($"Nombre del grupo: {grupoSkin.Key}");
                Console.WriteLine("==================================");
                PrintPeople(grupoSkin);
            }

        }

        private static void PrintPeople(IEnumerable<Person> peopleParam)
        {
            foreach (var person in peopleParam)
            {
                Console.WriteLine($"Name: {person.Name} - Height: {person.Height} - Mass: {person.Mass} - Skin Color: {person.SkinColor}");
            }
        }

        // en esta lista vamos a guardar todos los registros de las personas
        static List<Person> people;

        static void GetPeople(string nextUrl)
        {
            if (people == null)
            {
                people = new List<Person>();
            }

            if (nextUrl == null)
            {
                return;
            }

            // 1. Crear el cliente http
            var client = new HttpClient();

            // 2. Obtener la respuesta del servicio https://swapi.co/api/people
            var respuesta = client.GetAsync(nextUrl).Result;

            // 3. Leer el contenido de la respuesta como texto
            var respuestaTexto = respuesta.Content.ReadAsStringAsync().Result;

            // 4. Deserializar la respuesta que está en JSON
            var respuestaDeserializada = JsonConvert.DeserializeObject<PeopleResult>(respuestaTexto);

            // Pintar la data utilizando un foreach
            //Console.WriteLine($"Cantidad de registro: {respuestaDeserializada.Count}");
            //Console.WriteLine($"Página siguiente: {respuestaDeserializada.Next}");

            foreach (var person in respuestaDeserializada.Results)
            {
                //Console.WriteLine($"Name: {person.Name} - Height: {person.Height} - Mass: {person.Mass}");
                // agregar cada resultado a la lista people
                people.Add(person);
            }

            // volver a llamar a esta funcion
            GetPeople(respuestaDeserializada.Next);
        }
    }
}
