﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.Models
{
    public class Track
    {
        public Track()
        {
            InvoiceLines = new HashSet<InvoiceLine>();
        }
        // declarar las propiedades de la clase
        public int TrackId { get; set; }
        public string Name { get; set; }
        public int? AlbumId { get; set; }
        public int MediaTypeId { get; set; }
        public int? GenreId { get; set; }
        public string Composer { get; set; }
        public int Milliseconds { get; set; }
        public int? Bytes { get; set; }
        public decimal UnitPrice { get; set; }

        public ICollection<InvoiceLine> InvoiceLines { get; set; }
    }
}
