﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Dni { get; set; }
        public string Name { get; set; }
        public int? CustomerId { get; set; }
        public Customer Customer { get; set; }
        public string Roles { get; set; }
    }
}
