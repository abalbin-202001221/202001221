﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.Models.DataContracts
{
    [DataContract]
    public class TrackSalesDataContract
    {
        [DataMember]
        public int TrackId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int SalesCount { get; set; }
    }
}
