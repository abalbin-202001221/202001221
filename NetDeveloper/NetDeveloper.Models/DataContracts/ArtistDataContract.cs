﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace NetDeveloper.Models.DataContracts
{
    [DataContract]
    public class ArtistDataContract
    {
        [DataMember]
        public int ArtistId { get; set; }
        [DataMember]
        public string Name { get; set; }
    }
}
