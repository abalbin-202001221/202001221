﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.Models
{
    public class Artist
    {
        public Artist()
        {
            Albums = new HashSet<Album>();
        }
        public int ArtistId { get; set; }
        public string Name { get; set; }

        // esta propiedad indica que un artista puede tener muchos álbumes
        public ICollection<Album> Albums { get; set; }
    }
}
