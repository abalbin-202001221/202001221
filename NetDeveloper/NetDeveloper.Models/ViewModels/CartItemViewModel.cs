﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.Models.ViewModels
{
    public class CartItemViewModel
    {
        public string Nombre { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public int TrackId { get; set; }
    }
}
