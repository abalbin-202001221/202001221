﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        // hacemos que el int sea nullable para que podamos insertar un nuevo customer con este campo en nulo
        public int? SupportRepId { get; set; }
        [Column("Img_Profile")]
        public string ImageProfile { get; set; }
        //public Employee SupportRep { get; set; }
        public DateTime? BirthDate { get; set; }
    }
}
