﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.Models.Reports
{
    public class AlbumArtistReport
    {
        public string AlbumTitle { get; set; }
        public string ArtistName { get; set; }
    }
}
