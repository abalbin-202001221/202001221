﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.Models.Reports
{
    public class ArtistAlbumCountReport
    {
        public int ArtistId { get; set; }
        public string ArtistName { get; set; }
        public int AlbumCount { get; set; }
    }
}
