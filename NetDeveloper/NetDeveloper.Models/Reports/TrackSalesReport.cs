﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetDeveloper.Models.Reports
{
    public class TrackSalesReport
    {
        public int TrackId { get; set; }
        public string Name { get; set; }
        public int SalesCount { get; set; }
    }
}
