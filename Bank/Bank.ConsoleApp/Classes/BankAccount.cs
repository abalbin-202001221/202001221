﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.ConsoleApp.Classes
{
    public class BankAccount
    {
        // nombre del dueño de la cuenta
        public string OwnerName { get; set; }
        // número de la cuenta (único)
        public string Number { get; }
        // balance de cuenta
        public decimal Balance
        {
            get
            {
                // sumar las transacciones y devolver la suma total
                var currentBalance = 0M;
                foreach (var transaction in transactions)
                {
                    currentBalance += transaction.Amount;
                }

                // retornar current balance, ya que tendrá el valor de la suma
                return currentBalance;
            }
        }

        /// <summary>
        /// Lista de uso interno que servirá para calcular el balance y para alamacenar las transacciones en esta cuenta bancaria
        /// </summary>
        private List<Transaction> transactions = new List<Transaction>();

        static int accountNumberSeed = 1234567890;

        // contructor de la clase
        public BankAccount(string name, decimal initialBalance)
        {
            this.OwnerName = name;
            //this.Balance = initialBalance;
            // (old) registrar la transacción inicial. DateTime.Now sirve para obtener la fecha actual            
            //transactions.Add(new Transaction(initialBalance, DateTime.Now, "Apertura de cuenta"));
            // realizar el primer depósito de la cuenta
            Deposit(initialBalance, DateTime.Now, "Apertura de cuenta");
            // cada vez que se cree una instancia de la clase, sumar 1 al valor semilla del número de cuenta
            this.Number = accountNumberSeed.ToString();
            accountNumberSeed++;
        }

        /// <summary>
        /// Función para realizar un depósito a la cuenta bancaria
        /// </summary>
        /// <param name="amount">monto del depósito</param>
        /// <param name="date"> fecha del depósito</param>
        /// <param name="note">notas sobre el depósito</param>
        public void Deposit(decimal amount, DateTime date, string note)
        {
            // validar que el monto de depósito es mayor a 0
            if (amount <= 0)
            {
                // botar un error de que el parámetro está fuera de rango
                throw new ArgumentOutOfRangeException(nameof(amount), "El monto debe ser mayor a 0");
            }

            // crear la transacción
            var txDeposit = new Transaction(amount, date, note);

            // agregar la transacción a la lista interna de transacciones de esta cuenta
            transactions.Add(txDeposit);
        }

        /// <summary>
        /// Función para realizar un retiro a la cuenta bancaria
        /// </summary>
        /// <param name="amount">monto del retiro</param>
        /// <param name="date"> fecha del retiro</param>
        /// <param name="note">notas sobre el retiro</param>
        public void Withdrawal(decimal amount, DateTime date, string note)
        {
            // validar que el monto de retiro es mayor a 0
            if (amount <= 0)
            {
                // botar un error de que el parámetro está fuera de rango
                throw new ArgumentOutOfRangeException(nameof(amount), "El monto debe ser mayor a 0");
            }

            // validar que el balance final sea positivo luego del retiro
            if (Balance - amount < 0)
            {
                // botar un error indicando que la operación no es válida
                throw new InvalidOperationException("No tienes suficientes fondos para realizar la transacción");
            }

            // crear la transacción
            var txWithdrawal = new Transaction(-amount, date, note);

            // agregar la transacción a la lista interna de transacciones de esta cuenta
            transactions.Add(txWithdrawal);
        }

        /// <summary>
        /// Función que devuelve un string formateado mostrando las transacciones realizadas en la cuenta
        /// </summary>
        /// <returns></returns>
        public string TransactionHistory()
        {
            var report = new StringBuilder();

            // agregar la línea del título del reporte
            report.AppendLine("Fecha     Monto     Notas");
            report.AppendLine("=========================");
            foreach (var item in transactions)
            {
                report.AppendLine($"{item.Date}     {item.Amount}     {item.Notes}");
            }

            return report.ToString();
        }
    }
}
