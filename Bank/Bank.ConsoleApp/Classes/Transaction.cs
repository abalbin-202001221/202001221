﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.ConsoleApp.Classes
{
    /// <summary>
    /// Esta clase se utilizará para realizar los depósitos y retiros en una cuenta bancaria
    /// </summary>
    public class Transaction
    {
        /// <summary>
        /// El monto de la transacción
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// La fecha en la que se realizó la transacción
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Las notas relevantes para la transacción
        /// </summary>
        public string Notes { get; set; }

        public Transaction(decimal amount, DateTime date, string notes)
        {
            this.Amount = amount;
            this.Date = date;
            this.Notes = notes;
        }
    }
}
