﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.ConsoleApp.Classes;

namespace Bank.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenido al Banco");
            // crear una nueva cuenta bancaria
            BankAccount cuenta1 = new BankAccount("Arturo", 10M);
            // mostrar el owner de la cuenta 1
            Console.WriteLine(cuenta1.OwnerName);
            // mostrar el balance inicial de la cuenta 1
            Console.WriteLine($"El balance inicial de la cuenta 1 es: {cuenta1.Balance}");
            // mostrar el # de cuenta de la cuenta 1
            Console.WriteLine($"El # de cuenta de la cuenta 1 es: {cuenta1.Number}");
            // realizar un depósito a la cuenta 1 por 100 soles
            cuenta1.Deposit(100, DateTime.Now, "cobro de alquiler");
            // realizar un retiro de 54 soles de la cuenta 1
            cuenta1.Withdrawal(54, DateTime.Now, "pago de gasolina");            
            // mostrar el balance final de la cuenta luego de realizar las transacciones
            Console.WriteLine($"El balance final luego de realizar las transacciones es de: ${cuenta1.Balance}");

            // tratar de realizar un retiro con un monto que supere el balance actual de la cuenta 1
            try
            {
                cuenta1.Withdrawal(5400, DateTime.Now, "pagar cuota de carro"); 
            }
            catch (InvalidOperationException ex)
            {
                // mostrar el mensaje que nos devuelve´la excepción del método
                Console.WriteLine(ex.Message);
            }

            // imprimir las transacciones
            Console.WriteLine(cuenta1.TransactionHistory());

            // crear una cuenta 2
            var cuenta2 = new BankAccount("María", 1000M);
            // mostrar el siguiente mensaje: "La cuenta 2 pertenece a María 4"
            Console.WriteLine($"La cuenta 2 pertenece a {cuenta2.OwnerName} {2 + 2}");
            // mostrar el # de cuenta de la cuenta 2
            Console.WriteLine($"El # de cuenta de la cuenta 2 es: {cuenta2.Number}");

            // tratar de crear una cuenta con un monto inicial negativo
            try
            {
                var cuentaInvalida = new BankAccount("inválida", -200);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("El usuario trató de aperturar una cuenta con un saldo negativo");
                Console.WriteLine(ex.ToString());
            }

            // visualizar el valor del account number seed (solo si es public)
            //Console.WriteLine($"El valor actual del seed es: {BankAccount.accountNumberSeed}");

            //Console.ReadLine();
        }
    }
}
