﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NetDeveloperClients.WinForms.ArtistServiceReference {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ArtistDataContract", Namespace="http://schemas.datacontract.org/2004/07/NetDeveloper.Models.DataContracts")]
    [System.SerializableAttribute()]
    public partial class ArtistDataContract : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int ArtistIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ArtistId {
            get {
                return this.ArtistIdField;
            }
            set {
                if ((this.ArtistIdField.Equals(value) != true)) {
                    this.ArtistIdField = value;
                    this.RaisePropertyChanged("ArtistId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ArtistServiceReference.IArtistService")]
    public interface IArtistService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IArtistService/ObtenerTodo", ReplyAction="http://tempuri.org/IArtistService/ObtenerTodoResponse")]
        NetDeveloperClients.WinForms.ArtistServiceReference.ArtistDataContract[] ObtenerTodo();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IArtistService/ObtenerTodo", ReplyAction="http://tempuri.org/IArtistService/ObtenerTodoResponse")]
        System.Threading.Tasks.Task<NetDeveloperClients.WinForms.ArtistServiceReference.ArtistDataContract[]> ObtenerTodoAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IArtistService/Insertar", ReplyAction="http://tempuri.org/IArtistService/InsertarResponse")]
        int Insertar(NetDeveloperClients.WinForms.ArtistServiceReference.ArtistDataContract artist);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IArtistService/Insertar", ReplyAction="http://tempuri.org/IArtistService/InsertarResponse")]
        System.Threading.Tasks.Task<int> InsertarAsync(NetDeveloperClients.WinForms.ArtistServiceReference.ArtistDataContract artist);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IArtistServiceChannel : NetDeveloperClients.WinForms.ArtistServiceReference.IArtistService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ArtistServiceClient : System.ServiceModel.ClientBase<NetDeveloperClients.WinForms.ArtistServiceReference.IArtistService>, NetDeveloperClients.WinForms.ArtistServiceReference.IArtistService {
        
        public ArtistServiceClient() {
        }
        
        public ArtistServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ArtistServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ArtistServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ArtistServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public NetDeveloperClients.WinForms.ArtistServiceReference.ArtistDataContract[] ObtenerTodo() {
            return base.Channel.ObtenerTodo();
        }
        
        public System.Threading.Tasks.Task<NetDeveloperClients.WinForms.ArtistServiceReference.ArtistDataContract[]> ObtenerTodoAsync() {
            return base.Channel.ObtenerTodoAsync();
        }
        
        public int Insertar(NetDeveloperClients.WinForms.ArtistServiceReference.ArtistDataContract artist) {
            return base.Channel.Insertar(artist);
        }
        
        public System.Threading.Tasks.Task<int> InsertarAsync(NetDeveloperClients.WinForms.ArtistServiceReference.ArtistDataContract artist) {
            return base.Channel.InsertarAsync(artist);
        }
    }
}
