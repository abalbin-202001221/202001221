﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetDeveloperClients.WinForms
{
    public partial class InsertArtist : Form
    {
        public InsertArtist()
        {
            InitializeComponent();
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            var serviceClient = new ArtistServiceReference.ArtistServiceClient();
            var insertResult = serviceClient.Insertar(new ArtistServiceReference.ArtistDataContract { Name = textBox1.Text });
            MessageBox.Show($"Se insertó un nuevo artista con ID: {insertResult}");
        }
    }
}
