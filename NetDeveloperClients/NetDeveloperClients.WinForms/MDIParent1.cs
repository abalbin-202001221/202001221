﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetDeveloperClients.WinForms
{
    public partial class MDIParent1 : Form
    {
        private int childFormNumber = 0;

        public MDIParent1()
        {
            InitializeComponent();
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void printSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var demosChild = new Form1
            {
                MdiParent = this,
                Text = "Métodos de prueba con WCF"
            };
            demosChild.Show();
        }

        private void nuevoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var insertArtistChild = new InsertArtist
            {
                MdiParent = this,
                Text = "Insertar un artista"
            };
            insertArtistChild.Show();
        }
    }
}
