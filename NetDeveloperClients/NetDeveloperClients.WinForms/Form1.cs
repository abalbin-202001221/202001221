﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetDeveloperClients.WinForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // crear el cliente para invocar el servicio
            var serviceClient = new ArtistServiceReference.ArtistServiceClient();

            // obtener todos los artistas utilizando el servicio
            var data = serviceClient.ObtenerTodo();

            // convertir cada elemento una cadena de texto para mostrarla en el listbox
            var dataTexto = data.Select(d => $"Id: {d.ArtistId} - Name: {d.Name}");

            // enlazar con el listbox
            listBox1.DataSource = dataTexto.ToList();            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // crear el cliente para invocar el servicio
            var serviceClient = new TrackServiceReference.TrackServiceClient();

            // obtener la data utilizando el servicio
            var data = serviceClient.GetSalesReport();

            // convertir cada elemento una cadena de texto para mostrarla en el listbox
            var dataTexto = data.Select(d => $"Canción: {d.Name} - Cantidad: {d.SalesCount}");

            // enlazar con el listbox
            listBox1.DataSource = dataTexto.ToList();
        }
    }
}
